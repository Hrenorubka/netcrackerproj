import React from "react";
import './loginStyle.css'

class LoginInput extends React.Component {

    constructor(props) {
        super(props);

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(e) {
        this.props.handleInputChange(e.target.value);
    }

    render() {
        return(
            <div className='loginProps'>
                <h1 className='loginNameProps'>{this.props.name}</h1>
                <input className='loginProps' value = {this.props.value} onChange={this.handleInputChange}/>
            </div>
        );
    }
}

export default LoginInput;