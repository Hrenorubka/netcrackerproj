import React from "react";
import LoginInput from "./LoginInput";
import {MDBBtn} from "mdb-react-ui-kit";

class LoginPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            login : '',
            password : ''
        };

        this.handleChangeLogin = this.handleChangeLogin.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleLoginButtonClick = this.handleLoginButtonClick.bind(this);
        this.handleExitButtonClick = this.handleExitButtonClick.bind(this);

    }

    handleChangeLogin(log) {
        this.setState({login: log});
    }

    handleChangePassword(pass) {
        this.setState({password: pass});
    }

    handleExitButtonClick() {
        this.props.handleExitButtonClick();
    }

    async handleLoginButtonClick() {
        let LoginForm = {
            login: this.state.login,
            password: this.state.password
        };
        if (LoginForm.login === '' || LoginForm.password === '') {
            alert("You have not entered all information");
            return;
        }
        let response;
        try {
            response = await fetch('http://localhost:8080/auth',
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(LoginForm)
                })
        } catch (e) {
            alert("Server is not available now");
            return;
        }
        if (response.status >= 200 && response.status < 300) {
            const adminResponse = await fetch('http://localhost:8080/admin/check',
                {
                    credentials: 'same-origin',
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(LoginForm)
                });
            const body = await response.json();
            if (adminResponse.status >= 200 && adminResponse.status < 300) {
                this.props.handleClickLogin(body.token, false, this.state.login);
                this.props.handleClickAdminLogin();
            } else {
                this.props.handleClickLogin(body.token, true, this.state.login);
            }
            this.props.handleExitButtonClick();
        } else
            alert("This user does not exist");
    }



    render() {
        return (
            <div className='loginBigPlace'>
                <MDBBtn outline color='danger' className='LoginExit' onClick={this.handleExitButtonClick}><h6>X</h6></MDBBtn>
                <div className='loginHeap'>
                    <h1 className='loginHeap'>Input your login and password:</h1>
                </div>
                <LoginInput name = {'Login:'} value = {this.state.login} handleInputChange = {this.handleChangeLogin}/>
                <LoginInput name = {'Password:'} value = {this.state.password} handleInputChange = {this.handleChangePassword}/>
                <MDBBtn outline color='black' className='loginButton' onClick={this.handleLoginButtonClick}><h3>Log in</h3></MDBBtn>
            </div>
        );
    }
}

export default LoginPage;