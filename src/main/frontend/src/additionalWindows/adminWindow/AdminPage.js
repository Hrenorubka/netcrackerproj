import React from "react";
import './AdminPage.css';
import AdminCarTable from "./AllCars/Table/AdminCarTable";
import AdminAllCarPlace from "./AllCars/AdminAllCarPlace";
import AdminAllServicePlace from "./AllService/AdminAllServicePlace";
import AdminAllParkingPointPlace from "./AllParkingPoints/AdminAllParkingPointPlace";
import {MDBBtn} from "mdb-react-ui-kit";

class AdminPage extends React.Component {
    constructor(props) {
        super(props);
        this.handleClickLogOut = this.handleClickLogOut.bind(this);
    }

    handleClickLogOut() {
        this.props.handleChangePage('Main');
        this.props.handleChangeLogOut('', '', false);
    }

    render() {
        return (
          <div className='AdminBigPlace'>
              <div className='AdminHead'>
                  <h1 className='AdminHead'>
                      Admin page
                  </h1>
                  <MDBBtn rounded outline color='black' key = {"LogOutButton"} className='AdminLogOut' onClick={this.handleClickLogOut}>
                      <h3>Log Out</h3>
                  </MDBBtn>
              </div>

              <AdminAllCarPlace login = {this.props.login} token = {this.props.token}/>
              <AdminAllServicePlace login = {this.props.login} token = {this.props.token}/>
              <AdminAllParkingPointPlace login = {this.props.login} token = {this.props.token}/>
          </div>
        );
    }
}

export default AdminPage;