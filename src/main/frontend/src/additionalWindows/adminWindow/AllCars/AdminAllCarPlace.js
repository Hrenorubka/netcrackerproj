import React from "react";
import AdminCarTable from "./Table/AdminCarTable";
import AdminAllCarControllers from "./AdminAllCarControllers";
import AdminAllCarInputs from "./AdminAllCarInputs";

class AdminAllCarPlace extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            carsInfo : [],
            state : 0,
            updated : 0
        }

        this.changeState = this.changeState.bind(this);
        this.changeUpdated = this.changeUpdated.bind(this);
        this._isMounted = false;
    }

    async componentDidMount() {
        this._isMounted = true;
        const responseCars = await fetch( 'http://localhost:8080/admin/getAllCarsInfo',
            {
                credentials: 'same-origin',
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                }
            });
        if (responseCars.status >= 200 && responseCars.status < 300) {
            const body = await responseCars.json();
            if (this._isMounted) {
                this.setState({carsInfo: body});
            }
        }
    }


    async componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        if (this.state.updated !== prevState.updated) {
            this.setState({updated : 0});
            const responseCars = await fetch( 'http://localhost:8080/admin/getAllCarsInfo',
                {
                    credentials: 'same-origin',
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this.props.token
                    }
                });
            if (responseCars.status >= 200 && responseCars.status < 300) {
                const body = await responseCars.json();
                    this.setState({carsInfo: body});

            }
        }
    }
    componentWillUnmount() {
        this._isMounted = false;
    }

    changeState(newState) {
        this.setState({state : newState});
    }

    changeUpdated() {
        this.setState({updated : 1})
    }

    render() {
        return (
            <div className='AdminAllCarPlace'>
                <h1 className='AdminAllCarPlaceName'>Information about cars:</h1>
                <AdminCarTable login = {this.props.login} token = {this.props.token}
                               carsInfo = {this.state.carsInfo}/>
                <AdminAllCarControllers login = {this.props.login} token = {this.props.token}
                                        changeState = {this.changeState}/>
                <AdminAllCarInputs login = {this.props.login} token = {this.props.token}
                                   state = {this.state.state} changeUpdated = {this.changeUpdated}/>
            </div>
        );
    }

}

export default AdminAllCarPlace;