import React from "react";
import '../AdminPage.css'
import AdminAllCarInsert from "./Inputs/AdminAllCarInsert";
import AdminSelectInfo from "./Inputs/AdminSelectInfo";

class AdminAllCarInputs extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            carId : '',
            carNumber : '',
            parkingPoints : [],
            parkingPoint : '',
            ownerPhone : ''
        };

        this._isMounted = false;
        this.handleClickDelete = this.handleClickDelete.bind(this);
        this.handleClickUpdateButton = this.handleClickUpdateButton.bind(this);
        this.handleCarIdChange = this.handleCarIdChange.bind(this);
        this.handleParkingPointChange = this.handleParkingPointChange.bind(this);
        this.handleOwnerNameChange = this.handleOwnerNameChange.bind(this);

    }

    async componentDidMount() {
        this._isMounted = true;

        const responseParkingPoints = await fetch('http://localhost:8080/parkingPoint/all',
            {
                credentials: 'same-origin',
                method : 'GET',
                headers : {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization' : 'Bearer ' + this.props.token
                }
            });
        if (responseParkingPoints.status >= 200 && responseParkingPoints.status < 300) {
            const bodyParkingPoints = await responseParkingPoints.json();
            if (this._isMounted) {
                this.setState({parkingPoints: bodyParkingPoints});
            }
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleCarIdChange(e) {
        this.setState({carId : e.target.value});
    }

    handleParkingPointChange (e) {
        this.setState({parkingPoint : e.target.value});
    }

    handleOwnerNameChange (e) {
        this.setState({ownerPhone: e.target.value});
    }


    async handleClickUpdateButton() {
        let updateInfo = {
            carId : this.state.carId,
            parkingPoint : this.state.parkingPoint,
            ownerPhone : this.state.ownerPhone
        }
        if (this.state.carId === '') {
            alert("You have not entered car id");
            return;
        }
        if (this.state.parkingPoint === '') {
            alert("You have not entered parking spot");
            return;
        }
        if (this.state.ownerPhone === '') {
            alert("You have not entered owner phone");
            return;
        }
        if (this.state.ownerPhone.length !== 12) {
            alert("Incorrect phone number");
            return;
        }
        try {
            let response = await fetch('http://localhost:8080/admin/updateCarInfo', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                },
                method: 'POST',
                body: JSON.stringify(updateInfo)
            });
            if (response.status < 200 || response.status >= 300) {
                alert("Bad request");
                return;
            }
        } catch (e) {
            alert("Something went wrong");
            return;
        }
        this.props.changeUpdated();
    }

    async handleClickDelete() {
        if (this.state.carId === '') {
            alert("You have not entered car id");
            return;
        }
        try {
            let response = await fetch('http://localhost:8080/admin/deleteCar/' + this.state.carId, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                },
                method: 'POST'
            } );
            if (response.status < 200 || response.status >= 300) {
                alert("Bad request");
                return;
            }
        } catch (e) {
            alert("Something went wrong");
            return;
        }
        this.props.changeUpdated();
    }

    render() {

        let elem;
        if (this.props.state === 1) {
            elem = (
                <div className='AdminSpaceFiller'>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Car id:</h3>
                        <input className='AdminInputs' value={this.state.carId} onChange={this.handleCarIdChange}/>
                    </div>
                    <p></p>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Parking point:</h3>
                        <select className='AdminInputs' value={this.state.parkingPoint}
                                onChange={this.handleParkingPointChange}>
                            <AdminSelectInfo parkingPoint={this.state.parkingPoints}/>
                        </select>
                    </div>
                    <p></p>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Owner phone:</h3>
                        <input className='AdminInputs' onChange={this.handleOwnerNameChange}/>
                    </div>
                    <button className='AdminUpdateCar' onClick={this.handleClickUpdateButton}><h2>Push</h2></button>
                </div>
            );
        } else if (this.props.state === 2) {
            elem = (
                <div className='AdminSpaceFiller'>
                    <div className='AdminInputs'>
                    </div>
                    <p></p>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Car id:</h3>
                        <input className='AdminInputs' value={this.state.carId} onChange={this.handleCarIdChange}/>
                    </div>
                    <p></p>
                    <button className='AdminDeleteCar' onClick={this.handleClickDelete}><h2>Delete</h2></button>
                </div>
            );
        }
        return (
            <div className='AdminCarInputSection'>
                {elem}

            </div>
        );
    }
}

export default AdminAllCarInputs;