import React from "react";
import '../AdminPage.css'

class  AdminAllCarControllers extends React.Component {
    constructor(props) {
        super(props);

        this.handleClickUpdate = this.handleClickUpdate.bind(this);
        this.handleClickDelete = this.handleClickDelete.bind(this);
    }



    handleClickUpdate() {
        this.props.changeState(1);
    }

    handleClickDelete() {
        this.props.changeState(2);
    }

    render() {
        return (
            <div className='AdminCarReplaceSection'>
                <button className='AdminUpdateColumnButton' onClick={this.handleClickUpdate}>
                    <h4>Update car info</h4>
                </button>
                <button className='AdminDeleteColumnButton' onClick={this.handleClickDelete}>
                    <h4>Delete car</h4>
                </button>
            </div>
        );
    }
}

export default AdminAllCarControllers;