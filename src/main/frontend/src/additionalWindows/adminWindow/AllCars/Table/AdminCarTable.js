import React from "react";
import '../../AdminPage.css';
import AdminCarTableBody from "./AdminCarTableBody";


class AdminCarTable extends React.Component {
    constructor(props) {
        super(props);
    }



    render() {
        return (
            <div className='AdminCarTable'>
                <table className='AdminAllCar'>
                    <thead>
                        <th className='AdminAllCarTableHeap'>
                            Car Id
                        </th>
                        <th className='AdminAllCarTableHeap'>
                            Car number
                        </th>
                        <th className='AdminAllCarTableHeap'>
                            Parking Point
                        </th>
                        <th className='AdminAllCarTableHeap'>
                            Owner name
                        </th>
                        <th className='AdminAllCarTableHeap'>
                            Owner phone
                        </th>
                    </thead>
                    <AdminCarTableBody carsInfo = {this.props.carsInfo}/>
                </table>
            </div>
        );
    }
}

export default AdminCarTable;