import React from "react";


class AdminCarTableRow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <tr key = {this.props.car.entityId} className='AdminAllCarTableHeap'>
                <td className='AdminAllCarTableHeap'>
                    {this.props.car.entityId}
                </td>
                <td className='AdminAllCarTableHeap'>
                    {this.props.car.name}
                </td>
                <td className='AdminAllCarTableHeap'>
                    {this.props.parkingPoint.name}
                </td>
                <td className='AdminAllCarTableHeap'>
                    {this.props.user.username}
                </td>
                <td className='AdminAllCarTableHeap'>
                    {this.props.user.phoneNumber}
                </td>
            </tr>
        );
    }

}

export default AdminCarTableRow;