import React from "react";
import AdminCarTableRow from "./AdminCarTableRow";


class AdminCarTableBody extends React.Component {

    constructor(props) {
        super(props);
    }



    render() {
        const {carsInfo} = this.props;
        const elem = (
            carsInfo.map(carInfo =>
                <AdminCarTableRow key={carInfo.car.entityId} car={carInfo.car}
                                  parkingPoint={carInfo.parkingPoint} user={carInfo.user}/>
            )
        );
        return (<tbody className='AdminAllCarTableHeap'>{elem}</tbody>);
    }
}

export default AdminCarTableBody;