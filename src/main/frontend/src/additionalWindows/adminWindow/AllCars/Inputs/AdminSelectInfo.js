import React from "react";

class AdminSelectInfo extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {parkingPoint} = this.props;
        const elem = (
            parkingPoint.map(point =>
                <option value={point.name}>{point.name}</option>
            )
        );
        return elem;
    }

}

export default AdminSelectInfo;