import React from "react";

class AdminAllParkingPointControllers extends React.Component {
    constructor(props) {
        super(props);


        this.handleClickUpdateName = this.handleClickUpdateName.bind(this);
        this.handleClickUpdateNumberOfPassenger = this.handleClickUpdateNumberOfPassenger.bind(this);
        this.handleClickUpdateNumberOfCargo = this.handleClickUpdateNumberOfCargo.bind(this);
        this.handleClickUpdateName = this.handleClickUpdateName.bind(this);
        this.handleClickDelete = this.handleClickDelete.bind(this);
        this.handleClickInsert = this.handleClickInsert.bind(this);

    }

    handleClickUpdateName() {
        this.props.changeState(1);
    }

    handleClickUpdateNumberOfPassenger() {
        this.props.changeState(2);
    }

    handleClickUpdateNumberOfCargo() {
        this.props.changeState(3);
    }

    handleClickDelete() {
        this.props.changeState(4);
    }

    handleClickInsert() {
        this.props.changeState(5);
    }

    render() {
        return (
            <div className='AdminCarReplaceSection'>
                <button className='AdminUpdateNameParkingPointButton' onClick={this.handleClickUpdateName}>
                    <h4>Update name of parking points</h4>
                </button>
                <button className='AdminUpdatePassengerParkingPointButton' onClick={this.handleClickUpdateNumberOfPassenger}>
                    <h4>Update number of passenger seats</h4>
                </button>
                <button className='AdminUpdateCargoParkingPointButton' onClick={this.handleClickUpdateNumberOfCargo}>
                    <h4>Update number of cargo seats</h4>
                </button>
                <button className='AdminInsertParkingPointButton' onClick={this.handleClickDelete}>
                    <h4>Delete parking point</h4>
                </button>
                <button className='AdminDeleteParkingPointButton' onClick={this.handleClickInsert}>
                    <h4>Create parking point</h4>
                </button>
            </div>
        );
    }

}

export default AdminAllParkingPointControllers;