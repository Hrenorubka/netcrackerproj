import React from "react";

class AdminAllParkingPointInputs extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name : '',
            id : '',
            numberOfCargo : '',
            numberOfPassenger : ''
        };

        this.handleParkingPointIdChange = this.handleParkingPointIdChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleNumberOfCargoChange = this.handleNumberOfCargoChange.bind(this);
        this.handleNumberOfPassengerChange = this.handleNumberOfPassengerChange.bind(this);

        this.handleClickUpdateCargoButton = this.handleClickUpdateCargoButton.bind(this);
        this.handleClickUpdatePassengerButton = this.handleClickUpdatePassengerButton.bind(this);
        this.handleClickUpdateNameButton = this.handleClickUpdateNameButton.bind(this);
        this.handleClickDeleteButton = this.handleClickDeleteButton.bind(this);
        this.handleClickInsertButton = this.handleClickInsertButton.bind(this);

    }

    handleNameChange(e) {
        this.setState({name : e.target.value});
    }

    handleParkingPointIdChange(e) {
        this.setState({id : e.target.value});
    }

    handleNumberOfCargoChange(e) {
        this.setState({numberOfCargo : e.target.value});
    }

    handleNumberOfPassengerChange(e) {
        this.setState({numberOfPassenger : e.target.value});
    }

    async handleClickUpdateNameButton() {
        let parkingPointPrimitive = {
            parkingPoint : {
                entityId : this.state.id,
                entityType : '',
                name : this.state.name
            },
            numberOfCargoAll : this.state.numberOfCargo,
            numberOfPassengerAll : this.state.numberOfPassenger,
            numberOfCargoFree : '',
            numberOfPassengerFree : ''
        }
        if (this.state.id === '') {
            alert("You have not entered parking spot id");
            return;
        }
        if (this.state.name === '') {
            alert("You have not entered parking spot name");
            return;
        }
        try {
            let response = await fetch('http://localhost:8080/admin/updateParkingPoint/name', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                },
                method: 'POST',
                body: JSON.stringify(parkingPointPrimitive)
            });

            if (response.status < 200 || response.status >= 300) {
                alert("Bad request");
                return;
            }
        } catch (e) {
            alert("Bad request")
            return;
        }
        this.props.update();
    }

    async handleClickUpdateCargoButton() {
        let parkingPointPrimitive = {
            parkingPoint : {
                entityId : this.state.id,
                entityType : '',
                name : this.state.name
            },
            numberOfCargoAll : this.state.numberOfCargo,
            numberOfPassengerAll : this.state.numberOfPassenger,
            numberOfCargoFree : '',
            numberOfPassengerFree : ''
        }
        if (this.state.id === '') {
            alert("You have not entered parking spot id");
            return;
        }
        if (this.state.numberOfCargo === '') {
            alert("You have not entered number of cargo spots");
            return;
        }
        try {
            let response = await fetch('http://localhost:8080/admin/updateParkingPoint/cargo', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                },
                method: 'POST',
                body: JSON.stringify(parkingPointPrimitive)
            });

            if (response.status < 200 || response.status >= 300) {
                alert("Bad request");
                return;
            }
        } catch (e) {
            alert("Bad request")
            return;
        }
        this.props.update();
    }

    async handleClickUpdatePassengerButton() {
        let parkingPointPrimitive = {
            parkingPoint : {
                entityId : this.state.id,
                entityType : '',
                name : this.state.name
            },
            numberOfCargoAll : this.state.numberOfCargo,
            numberOfPassengerAll : this.state.numberOfPassenger,
            numberOfCargoFree : '',
            numberOfPassengerFree : ''
        }
        if (this.state.id === '') {
            alert("You have not entered parking spot id");
            return;
        }
        if (this.state.numberOfPassenger === '') {
            alert("You have not entered number of passenger spots");
            return;
        }
        try {
            let response = await fetch('http://localhost:8080/admin/updateParkingPoint/passenger', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                },
                method: 'POST',
                body: JSON.stringify(parkingPointPrimitive)
            });

            if (response.status < 200 || response.status >= 300) {
                alert("Bad request");
                return;
            }
        } catch (e) {
            alert("Bad request")
            return;
        }
        this.props.update();
    }

    async handleClickDeleteButton() {
        if (this.state.id === '') {
            alert("You have not entered parking spot id");
            return;
        }
        try {
            let response = await fetch(
                'http://localhost:8080/admin/updateParkingPoint/delete/' + this.state.id, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this.props.token
                    },
                    method: 'POST'
                });
            if (response.status === 405) {
                alert("Parking point has cars");
                return;
            }
            if (response.status < 200 || response.status >= 300) {
                alert("Bad request");
                return;
            }
        } catch (e) {
            alert("Bad request")
            return;
        }
        this.props.update();
    }

    async handleClickInsertButton() {
        let parkingPointPrimitive = {
            parkingPoint : {
                entityId : this.state.id,
                entityType : '',
                name : this.state.name
            },
            numberOfCargoAll : this.state.numberOfCargo,
            numberOfPassengerAll : this.state.numberOfPassenger,
            numberOfCargoFree : '',
            numberOfPassengerFree : ''
        }
        if (this.state.numberOfPassenger === '') {
            alert("You have not entered number of passenger spots");
            return;
        }
        if (this.state.name === '') {
            alert("You have not entered parking spot name");
            return;
        }
        if (this.state.numberOfCargo === '') {
            alert("You have not entered number of cargo spots");
            return;
        }
        try {
            let response = await fetch('http://localhost:8080/admin/updateParkingPoint/create', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                },
                method: 'POST',
                body: JSON.stringify(parkingPointPrimitive)
            });
            if (response.status < 200 || response.status >= 300) {
                alert("Bad request");
                return;
            }
        } catch (e) {
            alert("Bad request");
            return;
        }
        this.props.update();
    }


    render() {
        let elem;
        if (this.props.state === 1) {
            elem = (
                <div className='AdminSpaceFiller'>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Parking point id:</h3>
                        <input className='AdminInputs' value={this.state.id} onChange={this.handleParkingPointIdChange}/>
                    </div>
                    <p></p>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>New name:</h3>
                        <input className='AdminInputs' value={this.state.name} onChange={this.handleNameChange}/>
                    </div>
                    <button className='AdminUpdateCar' onClick={this.handleClickUpdateNameButton}><h2>Update</h2></button>
                </div>
            );
        } else if (this.props.state === 2) {
            elem = (
                <div className='AdminSpaceFiller'>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Parking point id:</h3>
                        <input className='AdminInputs' value={this.state.id} onChange={this.handleParkingPointIdChange}/>
                    </div>
                    <p></p>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Number of passenger:</h3>
                        <input className='AdminInputs' value={this.state.numberOfPassenger} onChange={this.handleNumberOfPassengerChange}/>
                    </div>
                    <button className='AdminDeleteCar' onClick={this.handleClickUpdatePassengerButton}><h2>Update</h2></button>
                </div>
            );
        } else if (this.props.state === 3) {
            elem = (
                <div className='AdminSpaceFiller'>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Parking point id:</h3>
                        <input className='AdminInputs' value={this.state.id} onChange={this.handleParkingPointIdChange}/>
                    </div>
                    <p></p>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Number of cargo:</h3>
                        <input className='AdminInputs' value={this.state.numberOfCargo} onChange={this.handleNumberOfCargoChange}/>
                    </div>
                    <button className='AdminUpdateCar' onClick={this.handleClickUpdateCargoButton}><h2>Update</h2></button>
                </div>
            );
        } else if (this.props.state === 4) {
            elem = (
                <div className='AdminSpaceFiller'>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Parking point id:</h3>
                        <input className='AdminInputs' value={this.state.id} onChange={this.handleParkingPointIdChange}/>
                    </div>
                    <button className='AdminUpdateCar' onClick={this.handleClickDeleteButton}><h2>Delete</h2></button>
                </div>
            );
        } else if (this.props.state === 5) {
            elem = (
                <div className='AdminSpaceFiller'>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>New name:</h3>
                        <input className='AdminInputs' value={this.state.name} onChange={this.handleNameChange}/>
                    </div>
                    <p></p>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Number of cargo:</h3>
                        <input className='AdminInputs' value={this.state.numberOfCargo} onChange={this.handleNumberOfCargoChange}/>
                    </div>
                    <p></p>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Number of passenger:</h3>
                        <input className='AdminInputs' value={this.state.numberOfPassenger} onChange={this.handleNumberOfPassengerChange}/>
                    </div>
                    <button className='AdminUpdateCar' onClick={this.handleClickInsertButton}><h2>Create</h2></button>
                </div>
            );
        }
        return (
            <div className='AdminCarInputSection'>
                {elem}
            </div>
        );
    }
}

export default AdminAllParkingPointInputs;