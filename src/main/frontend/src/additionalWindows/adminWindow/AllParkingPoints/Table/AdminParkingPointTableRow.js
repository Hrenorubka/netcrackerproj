import React from "react";

class AdminParkingPointTableRow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <tr key = {this.props.parkingPoint.entityId} className='AdminAllCarTableHeap'>
                <td className='AdminAllCarTableHeap'>
                    {this.props.parkingPoint.entityId}
                </td>
                <td className='AdminAllCarTableHeap'>
                    {this.props.parkingPoint.name}
                </td>
                <td className='AdminAllCarTableHeap'>
                    {this.props.numberOfCargoAll}
                </td>
                <td className='AdminAllCarTableHeap'>
                    {this.props.numberOfCargoFree}
                </td>
                <td className='AdminAllCarTableHeap'>
                    {this.props.numberOfPassengerAll}
                </td>
                <td className='AdminAllCarTableHeap'>
                    {this.props.numberOfPassengerFree}
                </td>
            </tr>
        );
    }

}

export default AdminParkingPointTableRow;