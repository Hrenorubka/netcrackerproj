import React from "react";
import AdminParkingPointTableRow from "./AdminParkingPointTableRow";

class AdminParkingPointTableBody extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {parkingPoints} = this.props;
        const elem = (
            parkingPoints.map(parkingPoint =>
                <AdminParkingPointTableRow key={parkingPoint.parkingPoint.entityId} parkingPoint={parkingPoint.parkingPoint}
                                      numberOfCargoAll={parkingPoint.numberOfCargoAll} numberOfPassengerAll={parkingPoint.numberOfPassengerAll}
                                      numberOfCargoFree={parkingPoint.numberOfCargoFree} numberOfPassengerFree={parkingPoint.numberOfPassengerFree} />
            )
        );
        return (<tbody className='AdminAllCarTableHeap'>{elem}</tbody>);
    }

}

export default AdminParkingPointTableBody;