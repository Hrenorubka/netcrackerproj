import React from "react";
import AdminParkingPointTableBody from "./AdminParkingPointTableBody";

class AdminParkingPointTable extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div className='AdminCarTable'>
                <table className='AdminAllCar'>
                    <thead>
                    <th className='AdminAllCarTableHeap'>
                        Parking point Id
                    </th>
                    <th className='AdminAllCarTableHeap'>
                        Parking point name
                    </th>
                    <th className='AdminAllCarTableHeap'>
                        Number of cargo seats
                    </th>
                    <th className='AdminAllCarTableHeap'>
                        Number of free cargo seats
                    </th>
                    <th className='AdminAllCarTableHeap'>
                        Number of passenger seats
                    </th>
                    <th className='AdminAllCarTableHeap'>
                        Number of free passenger seats
                    </th>
                    </thead>
                    <AdminParkingPointTableBody parkingPoints={this.props.parkingPoints}/>
                </table>
            </div>
        );
    }
}

export default AdminParkingPointTable;