import React from "react";
import AdminParkingPointTable from "./Table/AdminParkingPointTable";
import AdminAllParkingPointControllers from "./AdminAllParkingPointControllers";
import AdminAllParkingPointInputs from "./AdminAllParkingPointInputs";

class AdminAllParkingPointPlace extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            parkingPoints : [],
            state : 0,
            updated : 0
        }

        this.changeState = this.changeState.bind(this);
        this.changeUpdated = this.changeUpdated.bind(this);
        this._isMounted = false;

    }

    async componentDidMount() {
        this._isMounted = true;
        const responseCars = await fetch( 'http://localhost:8080/admin/allParkingPoints',
            {
                credentials: 'same-origin',
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                }
            });
        if (responseCars.status >= 200 && responseCars.status < 300) {
            const body = await responseCars.json();
            if (this._isMounted) {
                this.setState({parkingPoints: body});
            }
        }
    }

    async componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        if (this.state.updated !== prevState.updated) {
            this.setState({updated : 0});
            const responseCars = await fetch( 'http://localhost:8080/admin/allParkingPoints',
                {
                    credentials: 'same-origin',
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this.props.token
                    }
                });
            if (responseCars.status >= 200 && responseCars.status < 300) {
                const body = await responseCars.json();
                this.setState({parkingPoints: body});
            }
        }
    }


    componentWillUnmount() {
        this._isMounted = false;
    }

    changeUpdated() {
        this.setState({updated : 1})
    }

    changeState(newState) {
        this.setState({state : newState});
    }

    render() {
        return (
            <div className='AdminAllParkingPointPlace'>
                <h1 className='AdminAllCarPlaceName'>Information about parking points:</h1>
                <AdminParkingPointTable login = {this.props.login} token = {this.props.token}
                                   parkingPoints = {this.state.parkingPoints}/>
                <AdminAllParkingPointControllers login = {this.props.login} token = {this.props.token}
                                        changeState = {this.changeState}/>
                <AdminAllParkingPointInputs login = {this.props.login} token = {this.props.token}
                                            state = {this.state.state} update = {this.changeUpdated}/>
            </div>
        );
    }

}

export default AdminAllParkingPointPlace;