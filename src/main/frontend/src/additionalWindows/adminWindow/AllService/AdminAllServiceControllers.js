import React from "react";

class AdminAllServiceControllers extends React.Component {
    constructor(props) {
        super(props);


        this.handleClickUpdate = this.handleClickUpdate.bind(this);
        this.handleClickDelete = this.handleClickDelete.bind(this);
        this.handleClickInsert = this.handleClickInsert.bind(this);

    }

    handleClickUpdate() {
        this.props.changeState(1);
    }

    handleClickDelete() {
        this.props.changeState(2);
    }

    handleClickInsert() {
        this.props.changeState(3);
    }

    render() {
        return (
            <div className='AdminCarReplaceSection'>
                <button className='AdminUpdateColumnButton' onClick={this.handleClickUpdate}>
                    <h4>Update service cost</h4>
                </button>
                <button className='AdminDeleteColumnButton' onClick={this.handleClickDelete}>
                    <h4>Delete service</h4>
                </button>
                <button className='AdminInsertColumnButton' onClick={this.handleClickInsert}>
                    <h4>Create service</h4>
                </button>
            </div>
        );
    }

}

export default AdminAllServiceControllers;