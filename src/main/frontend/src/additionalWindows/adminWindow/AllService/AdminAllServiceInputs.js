import React from "react";

class AdminAllServiceInputs extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cost : '',
            id : '',
            name : ''
        };

        this.handleServiceIdChange = this.handleServiceIdChange.bind(this);
        this.handleCostChange = this.handleCostChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);

        this.handleClickUpdateButton = this.handleClickUpdateButton.bind(this);
        this.handleClickDeleteButton = this.handleClickDeleteButton.bind(this);
        this.handleClickInsertButton = this.handleClickInsertButton.bind(this);

    }

    handleCostChange(e) {
        this.setState({cost : e.target.value});
    }

    handleServiceIdChange(e) {
        this.setState({id : e.target.value});
    }

    handleNameChange(e) {
        this.setState({name : e.target.value});
    }

    async handleClickUpdateButton() {
        let updateInfo = {
            id : this.state.id,
            name : '',
            cost : this.state.cost,
            flag : ''
        }
        if (updateInfo.id === '') {
            alert("You have not entered service id");
        }
        if (updateInfo.cost === '') {
            alert("You have not entered service cost");
        }
        try {
            let response = await fetch('http://localhost:8080/admin/updateService/cost', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                },
                method: 'POST',
                body: JSON.stringify(updateInfo)
            });
            if (response.status < 200 || response.status >= 300) {
                alert("Bad request");
                return;
            }
        }  catch (e) {
            alert("Something went wrong");
            return;
        }
        this.props.update();
    }

    async handleClickDeleteButton() {
        try {
            let response = await fetch(
                'http://localhost:8080/admin/updateService/delete/' + this.state.id, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                },
                method: 'POST'
            });
            if (response.status < 200 || response.status >= 300) {
                alert("Bad request");
                return;
            }
        }  catch (e) {
            alert("Something went wrong");
            return;
        }
        this.props.update();
    }

    async handleClickInsertButton() {
        let insertInfo = {
            id : '',
            name : this.state.name,
            cost : this.state.cost,
            flag : ''
        }
        if (insertInfo.name === '') {
            alert("You have not entered service name");
        }
        if (insertInfo.cost === '') {
            alert("You have not entered service cost");
        }
        try {
            let response = await fetch('http://localhost:8080/admin/updateService/create', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                },
                method: 'POST',
                body: JSON.stringify(insertInfo)
            });
                if (response.status < 200 || response.status >= 300) {
                alert("Bad request");
                return;
            }
        }  catch (e) {
            alert("Something went wrong");
            return;
        }
        this.props.update();
    }


    render() {
        let elem;
        if (this.props.state === 1) {
            elem = (
                <div className='AdminSpaceFiller'>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Service id:</h3>
                        <input className='AdminInputs' value={this.state.id} onChange={this.handleServiceIdChange}/>
                    </div>
                    <p></p>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>New cost:</h3>
                        <input className='AdminInputs' value={this.state.cost} onChange={this.handleCostChange}/>
                    </div>
                    <button className='AdminUpdateCar' onClick={this.handleClickUpdateButton}><h2>Update</h2></button>
                </div>
            );
        } else if (this.props.state === 2) {
            elem = (
                <div className='AdminSpaceFiller'>
                    <div className='AdminInputs'>
                    </div>
                    <p></p>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Service id:</h3>
                        <input className='AdminInputs' value={this.state.id} onChange={this.handleServiceIdChange}/>
                    </div>
                    <p></p>
                    <button className='AdminDeleteCar' onClick={this.handleClickDeleteButton}><h2>Delete</h2></button>
                </div>
            );
        } else if (this.props.state === 3) {
            elem = (
                <div className='AdminSpaceFiller'>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Service name:</h3>
                        <input className='AdminInputs' value={this.state.name} onChange={this.handleNameChange}/>
                    </div>
                    <p></p>
                    <div className='AdminInputs'>
                        <h3 className='AdminInputs'>Cost:</h3>
                        <input className='AdminInputs' value={this.state.cost} onChange={this.handleCostChange}/>
                    </div>
                    <button className='AdminUpdateCar' onClick={this.handleClickInsertButton}><h2>Create</h2></button>
                </div>
            );
        }
        return (
            <div className='AdminCarInputSection'>
                {elem}
            </div>
        );
    }

}

export default AdminAllServiceInputs;