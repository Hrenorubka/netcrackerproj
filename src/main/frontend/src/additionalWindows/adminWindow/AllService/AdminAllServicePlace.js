import React from "react";
import AdminServiceTable from "./Table/AdminServiceTable";
import AdminAllServiceControllers from "./AdminAllServiceControllers";
import AdminAllServiceInputs from "./AdminAllServiceInputs";

class AdminAllServicePlace extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            service : [],
            state : 0,
            updated : 0
        }

        this.changeState = this.changeState.bind(this);
        this.changeUpdated = this.changeUpdated.bind(this);
        this._isMounted = false;
    }


    async componentDidMount() {
        this._isMounted = true;
        const responseCars = await fetch( 'http://localhost:8080/admin/allService',
            {
                credentials: 'same-origin',
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                }
            });
        if (responseCars.status >= 200 && responseCars.status < 300) {
            const body = await responseCars.json();
            if (this._isMounted) {
                this.setState({service: body});
            }
        }
    }

    async componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        if (this.state.updated !== prevState.updated) {
            console.log("update");
            const responseCars = await fetch( 'http://localhost:8080/admin/allService',
                {
                    credentials: 'same-origin',
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this.props.token
                    }
                });
            if (responseCars.status >= 200 && responseCars.status < 300) {
                const body = await responseCars.json();
                this.setState({service: body});
            }
        }
    }


    componentWillUnmount() {
        this._isMounted = false;
    }

    changeUpdated() {
        this.setState({updated : 1})
    }

    changeState(newState) {
        this.setState({state : newState});
    }

    render() {
        return (
            <div className='AdminAllServicePlace'>
                <h1 className='AdminAllCarPlaceName'>Information about car service:</h1>
                <AdminServiceTable login = {this.props.login} token = {this.props.token}
                                   service = {this.state.service}/>
                <AdminAllServiceControllers login = {this.props.login} token = {this.props.token}
                                            changeState = {this.changeState}/>
                <AdminAllServiceInputs login = {this.props.login} token = {this.props.token}
                                   state = {this.state.state} update = {this.changeUpdated}/>
            </div>
        );
    }

}

export default AdminAllServicePlace;