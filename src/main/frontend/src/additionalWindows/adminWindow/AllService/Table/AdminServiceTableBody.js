import React from "react";
import AdminServiceTableRow from "./AdminServiceTableRow";

class AdminServiceTableBody extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {service} = this.props;
        const elem = (
            service.map(serv =>
                <AdminServiceTableRow key={serv.id} service={serv} />
            )
        );
        return (<tbody className='AdminAllCarTableHeap'>{elem}</tbody>);
    }
}

export default AdminServiceTableBody;