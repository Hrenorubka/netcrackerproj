import React from "react";
import AdminServiceTableBody from "./AdminServiceTableBody";

class AdminServiceTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='AdminCarTable'>
                <table className='AdminAllCar'>
                    <thead>
                    <th className='AdminAllCarTableHeap'>
                        Service Id
                    </th>
                    <th className='AdminAllCarTableHeap'>
                        Service Name
                    </th>
                    <th className='AdminAllCarTableHeap'>
                        Cost
                    </th>
                    </thead>
                    <AdminServiceTableBody service={this.props.service}/>
                </table>
            </div>
        );
    }

}

export default AdminServiceTable;