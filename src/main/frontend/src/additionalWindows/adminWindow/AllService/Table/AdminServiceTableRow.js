import React from "react";

class AdminServiceTableRow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <tr key = {this.props.service.id} className='AdminAllCarTableHeap'>
                <td className='AdminAllCarTableHeap'>
                    {this.props.service.id}
                </td>
                <td className='AdminAllCarTableHeap'>
                    {this.props.service.name}
                </td>
                <td className='AdminAllCarTableHeap'>
                    {this.props.service.cost}
                </td>
            </tr>
        );
    }
}

export default AdminServiceTableRow;