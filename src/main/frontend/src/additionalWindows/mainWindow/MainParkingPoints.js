import React from "react";
import MainParkingPointsTableRow from "./MainParkingPointsTableRow";
import './MainStyle.css'

class MainParkingPoints extends React.Component {

    constructor(props) {
        super(props);
    }
    render() {
        const {parkingPoints} = this.props;
        const elem = (
            parkingPoints.map(pp =>
                <MainParkingPointsTableRow key = {pp.parkingPoint.entityId} id = {pp.parkingPoint.entityId}
                                           name = {pp.parkingPoint.name}
                                           numbAllCargo = {pp.numberOfCargoAll}
                                           numbFreeCargo = {pp.numberOfCargoFree}
                                           numbAllPassenger = {pp.numberOfPassengerAll}
                                           numbFreePassenger = {pp.numberOfPassengerFree}/>
            )
        )
        return (elem);
    }


}

export default MainParkingPoints;