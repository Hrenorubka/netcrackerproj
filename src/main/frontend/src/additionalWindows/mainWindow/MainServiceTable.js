import React from 'react';
import './MainStyle.css'
import MainService from "./MainService";
class MainServiceTable extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {
        return(
            <table className='mainTables'>
                <thead>
                <th className='mainTables'>Service:</th>
                <th className='mainTables'>Price:</th>
                </thead>
                <tbody>
                <MainService service={this.props.service}/>
                </tbody>
            </table>
        );
    }
}

export default MainServiceTable;