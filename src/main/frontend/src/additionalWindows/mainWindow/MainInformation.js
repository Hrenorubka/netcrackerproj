import React from "react";
import MainServiceTable from "./MainServiceTable";
import './MainStyle.css'
import MainParkingPointsTable from "./MainParkingPointsTable";

class MainInformation extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            service : [],
            parkingPoints : []
        }
        this._isMounted = false;
    }

    async componentDidMount() {
        this._isMounted = true;
        let responseService;
        try {
            responseService = await fetch(
                'http://localhost:8080/service/all',
                {
                    credentials: 'same-origin',
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this.props.token
                    }
                });
        } catch (e) {
            this.props.handleClickError();
            return;
        }
        if (responseService.status >= 200 && responseService.status < 300)
        {
            const bodyService = await responseService.json();
            if (this._isMounted) {
                this.setState({service: bodyService});
            }
        }
        let responseParkingPoints;
        try {
            responseParkingPoints = await fetch('http://localhost:8080/parkingPoint/all/info',
                {
                    credentials: 'same-origin',
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this.props.token
                    }
                });
        } catch (e) {
            this.props.handleClickError();
            return;
        }
        if (responseParkingPoints.status >= 200 && responseParkingPoints.status < 300) {
            const bodyParkingPoints = await responseParkingPoints.json();
            if (this._isMounted) {
                this.setState({parkingPoints: bodyParkingPoints});
            }
        }

    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        return (
            <div className='mainInfoPlace'>
                <div className='mainServiceCost'>
                    <MainServiceTable service = {this.state.service}/>
                </div>
                <div className='mainParkingPointInfo'>
                    <MainParkingPointsTable parkingPoints = {this.state.parkingPoints}/>
                </div>
            </div>
        )
    }

}

export default MainInformation;