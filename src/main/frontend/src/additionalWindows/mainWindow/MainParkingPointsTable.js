import React from "react";
import MainParkingPoints from "./MainParkingPoints";
import './MainStyle.css'

class MainParkingPointsTable extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return(
            <table className='mainTables'>
                <thead>
                <th className='mainTables'>Parking lot:</th>
                <th className='mainTables'>Total passenger spots:</th>
                <th className='mainTables'>Available passenger spots:</th>
                <th className='mainTables'>Total cargo spots:</th>
                <th className='mainTables'>Available cargo spots:</th>
                </thead>
                <tbody>
                <MainParkingPoints parkingPoints={this.props.parkingPoints}/>
                </tbody>
            </table>
        );
    }

}


export default MainParkingPointsTable;