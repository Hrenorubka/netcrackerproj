import React from "react";
import './MainStyle.css'

class MainParkingPointsTableRow extends React.Component {

    constructor(props) {
        super(props);
    }

    render () {
        return (
            <tr className='mainTables' key={this.props.id}>
                <td className='mainTables'>{this.props.name}</td>
                <td className='mainTables'>{this.props.numbAllPassenger}</td>
                <td className='mainTables'>{this.props.numbFreePassenger}</td>
                <td className='mainTables'>{this.props.numbAllCargo}</td>
                <td className='mainTables'>{this.props.numbFreeCargo}</td>
            </tr>
        )
    }
}

export default MainParkingPointsTableRow;

