import React from "react";
import './MainStyle.css'

class  MainServiceTableRow extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <tr className='mainTables' key={this.props.id}>
                <td className='mainTables'>{this.props.name}</td>
                <td className='mainTables'>{this.props.cost + '₽'}</td>
            </tr>
        );
    }
}

export default MainServiceTableRow;
