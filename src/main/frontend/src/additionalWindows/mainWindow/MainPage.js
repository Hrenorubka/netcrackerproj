import React from "react";
import "./MainStyle.css"
import GiveCar from "../giveWindow/GiveCar";
import GetCar from "../getWindow/GetCar";
import LoginPage from "../loginWindow/LoginPage";
import RegistrationPage from "../registrationWindow/RegistrationPage";
import MainInformation from "./MainInformation";
import { MDBBtn } from 'mdb-react-ui-kit'

class MainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            flag: 0
        }

        this.handleClickGet = this.handleClickGet.bind(this);
        this.handleClickGive = this.handleClickGive.bind(this);
        this.handleClickLogin = this.handleClickLogin.bind(this);
        this.handleClickRegister = this.handleClickRegister.bind(this);
        this.handleClickGiveExit = this.handleClickGiveExit.bind(this);
        this.handleClickGetExit = this.handleClickGetExit.bind(this);
        this.handleClickLoginExit = this.handleClickLoginExit.bind(this);
        this.handleClickRegisterExit = this.handleClickRegisterExit.bind(this);
        this.handleClickLogOut = this.handleClickLogOut.bind(this);
        this.handleClickProfile = this.handleClickProfile.bind(this);
        this.handleClickAdminLogin = this.handleClickAdminLogin.bind(this);
        this.handleClickError = this.handleClickError.bind(this);
    }


    handleClickGive() {
        this.setState({flag: 1});
    }


    handleClickGet() {
        this.setState({flag: 2});
    }

    handleClickLogin(e, flagState, userLogin) {
        this.setState({flag: 3});
        this.props.handleChangeLogin(userLogin, e, flagState);

    }

    handleClickRegister() {
        this.setState({flag : 4});
    }

    handleClickGiveExit() {
        this.setState({flag: 0});
    }

    handleClickGetExit() {
        this.setState({flag: 0});
    }

    handleClickLoginExit() {
        this.setState({flag: 0});
    }

    handleClickRegisterExit() {
        this.setState({flag : 0});
    }

    handleClickLogOut() {
        this.props.handleChangeLogin('', '', false);
    }

    handleClickProfile() {
        this.props.handleChangePage('Profile');
    }

    handleClickAdminLogin() {
        this.props.handleChangePage('Admin');
    }

    handleClickError() {
        this.props.handleChangePage('Error');
    }


    render() {
        let elem;
        if (this.state.flag == 0) {
            elem = <MainInformation token = {this.props.token} handleClickError = {this.handleClickError}/>
        }
        else if (this.state.flag == 1) {
            elem = <GiveCar handleExitButtonClick={this.handleClickGiveExit} token = {this.props.token}/>
        } else if (this.state.flag == 2) {
            elem = <GetCar handleExitButtonClick={this.handleClickGetExit} token = {this.props.token}/>
        } else if (this.state.flag == 3) {
            elem = <LoginPage handleExitButtonClick = {this.handleClickLoginExit}
                              handleClickAdminLogin = {this.handleClickAdminLogin}
                              handleClickLogin = {this.handleClickLogin} token = {this.props.token}/>
        } else if (this.state.flag == 4) {
            elem = <RegistrationPage handleExitButtonClick = {this.handleClickRegisterExit} handleClickRegister = {this.handleClickRegister}/>
        }

        const guestHeap =  [
            <h1 key={"startName"}>Start Page</h1>,
            <MDBBtn outline rounded color='black' key = {"LoginButton"} className='mainLoginButton' onClick={this.handleClickLogin}>
                <h4>Login</h4>
            </MDBBtn>,
            <MDBBtn outline rounded color='black' key = {"RegistrationButton"} className='mainRegisterButton' onClick={this.handleClickRegister}>
                <h4>Registration</h4>
            </MDBBtn>
        ];

        const userHeap = [
            <h1 key={"startName"}>Welcome back!</h1>,
            <MDBBtn outline rounded color='black' key = {"Profile"} className='mainLoginButton' onClick={this.handleClickProfile}>
                <h4>Profile</h4>
            </MDBBtn>,
            <MDBBtn outline rounded color='black' key = {"LogOutButton"} className='mainRegisterButton' onClick={this.handleClickLogOut}>
                <h4>Log Out</h4>
            </MDBBtn>
        ];

        let heap = (this.props.heapFlag === false) ? guestHeap : userHeap;

        return (

            <div className='mainBigPlace'>
                <div className='mainHeap'>
                    {heap}
                </div>
                <div className='buttonsPlace'>
                    <MDBBtn outline color='black' className='leftButton' onClick={this.handleClickGive}><h3>Park a car</h3></MDBBtn>
                    <MDBBtn outline color='black' className='rightButton' onClick={this.handleClickGet}><h3>Take a car</h3></MDBBtn>
                </div>
                {elem}
            </div>
        );
    }
}

export default MainPage;
