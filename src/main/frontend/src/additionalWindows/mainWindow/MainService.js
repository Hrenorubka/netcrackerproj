import React from "react";
import MainServiceTableRow from "./MainServiceTableRow";
import './MainStyle.css'

class MainService extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {service} = this.props;
        const elem = (
            service.map(serv =>
                <MainServiceTableRow key={serv.id} id = {serv.id} name = {serv.name} cost = {serv.cost} />
            )
        )
        return (
            elem
        );
    }

}

export default MainService;