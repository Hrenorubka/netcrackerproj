import React from 'react';
import './GiveCar.css'
import './CarInfoSection.js'
import './CarServiceSection.js'
import CarInfoSection from "./CarInfoSection";
import CarServiceSection from "./CarServiceSection";
import SubmitButton from "./SubmitButton";
import {MDBBtn} from "mdb-react-ui-kit";

class GiveCar extends React.Component {

    constructor(props) {
        super(props);

        this.handleCheckboxClick = this.handleCheckboxClick.bind(this);
        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
        this.handleNameOfCarChange = this.handleNameOfCarChange.bind(this);
        this.handleParkingPointChange = this.handleParkingPointChange.bind(this);
        this.handleTypeOfCarChange = this.handleTypeOfCarChange.bind(this);
        this.handleExitButtonClick = this.handleExitButtonClick.bind(this);

        this._isMounted = false;

        this.state = {
            parkingPoint : '',
            nameOfCar : '',
            phoneNumber : '+7',
            typeOfCar : '',
            service: [],
            parkingPoints : []
        }
    }

    handlePhoneNumberChange(e) {
        this.setState({phoneNumber: e});
    }

    handleNameOfCarChange(e) {
        this.setState({nameOfCar: e});
    }

    handleParkingPointChange(e) {
        this.setState({parkingPoint: e});
    }

    handleTypeOfCarChange(e) {
        this.setState({typeOfCar: e});
    }

    handleCheckboxClick(i) {
        this.state.service[i].flag = !this.state.service[i].flag;
    }

    handleExitButtonClick() {
        this.props.handleExitButtonClick();
    }


    async componentDidMount() {
        this._isMounted = true;
        const responseService = await fetch('http://localhost:8080/service/all',
            {
                credentials: 'same-origin',
                method : 'GET',
                headers : {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization' : 'Bearer ' + this.props.token
                }
            });
        if (responseService.status >= 200 && responseService.status < 300)
        {
            const bodyService = await responseService.json();
            console.log(bodyService);
            if (this._isMounted) {
                this.setState({service: bodyService});
            }
        }
        const responseParkingPoints = await fetch('http://localhost:8080/parkingPoint/all',
            {
                credentials: 'same-origin',
                method : 'GET',
                headers : {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization' : 'Bearer ' + this.props.token
                }
            });
        if (responseParkingPoints.status >= 200 && responseParkingPoints.status < 300) {
            const bodyParkingPoints = await responseParkingPoints.json();
            if (this._isMounted) {
                this.setState({parkingPoints: bodyParkingPoints});
            }
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }


    render() {
        return (
            <div className='bigplace'>
                <MDBBtn outline color='danger' className='GiveExit' onClick={this.handleExitButtonClick}><h6>X</h6></MDBBtn>
                <CarInfoSection
                    parkingPoint={this.state.parkingPoint} handleParkingPointChange={this.handleParkingPointChange}
                    nameOfCar={this.state.nameOfCar} handleNameOfCarChange={this.handleNameOfCarChange}
                    phoneNumber={this.state.phoneNumber} handlePhoneNumberChange={this.handlePhoneNumberChange}
                    typeOfCar={this.state.typeOfCar} handleTypeOfCarChange={this.handleTypeOfCarChange}
                    parkingPoints={this.state.parkingPoints}
                />
                <CarServiceSection
                    service={this.state.service} handleClick={this.handleCheckboxClick}
                />

                <SubmitButton
                    service={this.state.service}
                    nameOfCar={this.state.nameOfCar}
                    phoneNumber={this.state.phoneNumber}
                    parkingPoint={this.state.parkingPoint}
                    typeOfCar={this.state.typeOfCar}
                />
            </div>
        );
    }
}

export default GiveCar;