import React from 'react';
import ServiceTable from "./Sections/ServiceTable";
import './GiveCar.css'
class CarInfoSection extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }


    handleClick(i) {
        this.props.handleClick(i);
    }


    render() {
        return(
            <div className='carServiceSection'>
                <div className='SectionHeap'>
                    <h1 className='NameHeap'>Select service:</h1>
                </div>
                <div className='SectionSelection'>
                    <ServiceTable service={this.props.service} handleClick={this.handleClick}/>
                </div>
            </div>
        );
    }

}

export default CarInfoSection;