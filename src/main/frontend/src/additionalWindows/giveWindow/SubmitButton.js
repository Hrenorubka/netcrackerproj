import React from "react";
import './GiveCar.css'
import {MDBBtn} from "mdb-react-ui-kit";
class SubmitButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {isToggleOn: true};
        this.handleClick = this.handleClick.bind(this);
    }

    async handleClick() {

        let carInfo = {
            carNumber: this.props.nameOfCar,
            dateEvent: new Date(),
            listServicesId: [],
            inPlace: true,
            carType: this.props.typeOfCar,
            parkingPoint: this.props.parkingPoint,
            phoneNumber: this.props.phoneNumber
        };
        const {service} = this.props;
        service.map(serv => serv.flag ? carInfo.listServicesId.push(serv.id) : false);
        if (carInfo.carNumber === '') {
            alert("You have not entered car number");
            return;
        }
        if (carInfo.parkingPoint === '') {
            alert("You have not entered parking point");
            return;
        }
        if (carInfo.carType === '') {
            alert("You have not entered car type");
            return;
        }
        if (carInfo.phoneNumber === '') {
            alert("You have not entered phone number");
            return;
        } else if (carInfo.phoneNumber.length !== 12) {
            alert("Incorrect phone number");
            return;
        }
        if (carInfo.listServicesId.length === 0) {
            alert("Select at least one service");
            return;
        }
        let response = await fetch('http://localhost:8080/car', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(carInfo)
        });
        if (response.status >= 200 && response.status < 300) {
            this.setState(prevState => ({
                isToggleOn: !prevState.isToggleOn
            }));
            let result = await response.json();
        } else {
            alert("Something went wrong");
        }
    }

    render() {
        return (
            <div className='SubmitButton'>
                <MDBBtn outline color='black' className='Push' onClick={this.handleClick}><h3>Confirm</h3></MDBBtn>
            </div>
        );
    }
}

export default SubmitButton;