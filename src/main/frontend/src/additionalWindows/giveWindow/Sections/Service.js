import React from "react";
import TableRow from "./TableRow";
import '../GiveCar.css'
class Service extends React.Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }


    handleClick(i) {
        // this.state.service[i].flag = !this.state.service[i].flag;
        this.props.handleClick(i);
    }

    render(){
        const {service} = this.props;
        var i = 0;
        const elem = (
            service.map(serv =>
                <TableRow key={serv.id} id = {serv.id} name = {serv.name} cost = {serv.cost} isToggleOn = {serv.flag} handleClick={this.handleClick} i={i++}/>
            )
        );
        return (
            elem
        );
    }
}

export default Service;

