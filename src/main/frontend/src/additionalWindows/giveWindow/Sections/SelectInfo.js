import React from "react";

class SelectInfo extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {parkingPoints} = this.props;
        const elem = (
            parkingPoints.map(point =>
                <option value={point.name}>{point.name}</option>
            )
        );
        return elem;
    }

}

export default SelectInfo