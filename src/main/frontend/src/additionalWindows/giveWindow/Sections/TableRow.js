import React from "react";
import '../GiveCar.css'
class TableRow extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isToggleOn: false
        };
        this.handleClick = this.handleClick.bind(this);
    }


    handleClick() {
        this.props.handleClick(this.props.i);
        // this.setState(prevState => ({
        //     isToggleOn: !prevState.isToggleOn
        // }));
    }

    render(){
        return (
            <tr className='Service' key={this.props.id}>
                <td className='Service'>{this.props.name}</td>
                <td className='Service'>{this.props.cost + ' ₽'}</td>
                <td className='Service'><input type="checkbox" value={this.props.isToggleOn} onClick={this.handleClick}/></td>
            </tr>
        );
    }
}

export default TableRow;

