import React from 'react';
import '../GiveCar.css'
class InputInfo extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        //this.state = {inputString: ''};

    }

    handleChange(e) {
        //this.setState({inputString: e.target.value});
        this.props.handleChange(e.target.value);
    }

    render() {
        return (
            <div className='Props'>
                <h1 className='NameProps'>{this.props.name}</h1>
                <input className='Props' value={this.props.inputString} onChange={this.handleChange}>
                    {/*{console.log(this.state.inputString)}*/}
                </input>
            </div>
        );
    }
}

export default InputInfo;