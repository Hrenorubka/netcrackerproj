import React from 'react';
import Service from "./Service";
import '../GiveCar.css'
class ServiceTable extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }


    handleClick(i) {
        this.props.handleClick(i);
    }

    render() {
        return(
            <table className='Service'>
                <thead>
                    <th className='Service'>Service:</th>
                    <th className='Service'>Price:</th>
                    <th className='Service'>Selected:</th>
                </thead>
                <tbody>
                    <Service service={this.props.service} handleClick={this.handleClick}/>
                </tbody>
            </table>
        );
    }
}

export default ServiceTable;