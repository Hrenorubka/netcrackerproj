import React from 'react';
import InputInfo from "./Sections/InputInfo";
import './GiveCar.css'
import SelectInfo from "./Sections/SelectInfo";
class CarInfoSection extends React.Component {

    constructor(props) {
        super(props);

        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
        this.handleNameOfCarChange = this.handleNameOfCarChange.bind(this);
        this.handleParkingPointChange = this.handleParkingPointChange.bind(this);
        this.handleTypeOfCarChange = this.handleTypeOfCarChange.bind(this);

    }

    handlePhoneNumberChange(e) {
        this.props.handlePhoneNumberChange(e);
    }

    handleNameOfCarChange(e) {
        this.props.handleNameOfCarChange(e);
    }

    handleParkingPointChange(e) {
        this.props.handleParkingPointChange(e.target.value);
        console.log(this.props.parkingPoint);
    }

    handleTypeOfCarChange(e) {
        this.props.handleTypeOfCarChange(e.target.value);
    }

    render() {
        return(
            <form className='carInfoSection'>
                <div className='SectionHeap'>
                    <h1 className='NameHeap'>Your information:</h1>
                </div>
                <div className='SectionSelection'>
                    <div className='Props'>
                        <h1 className='NameProps'>Parking point:</h1>
                        <select className='ParkingPoints' value = {this.props.parkingPoint} onChange={this.handleParkingPointChange}>
                            <SelectInfo parkingPoints = {this.props.parkingPoints} parkingPoint = {this.props.parkingPoint}/>
                        </select>
                    </div>
                    {/*<InputInfo name = "Parking point:" inputString = {this.props.parkingPoint} handleChange={this.handleParkingPointChange}/>*/}
                    <InputInfo name = "Car's number:" inputString = {this.props.nameOfCar} handleChange={this.handleNameOfCarChange}/>
                    <InputInfo name = "Your phone:" inputString = {this.props.phoneNumber} handleChange={this.handlePhoneNumberChange}/>
                    {/*<InputInfo name = "Type of a car:" inputString = {this.props.typeOfCar} handleChange={this.handleTypeOfCarChange}/>*/}
                    <div className='Props'>
                        <h1 className='NameProps'>Type of a car:</h1>
                        <select className='ParkingPoints' value = {this.props.typeOfCar} onChange={this.handleTypeOfCarChange}>
                            <option value = {'Passenger'}>{'Passenger'}</option>
                            <option value={'Cargo'}>{'Cargo'}</option>
                        </select>
                    </div>
                </div>
            </form>
        );
    }

}

export default CarInfoSection;