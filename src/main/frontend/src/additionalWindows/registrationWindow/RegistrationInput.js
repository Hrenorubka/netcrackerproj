import React from "react";
import './RegistrationStyles.css'

class RegistrationInput extends React.Component {

    constructor(props) {
        super(props);

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(e) {
        this.props.handleInputChange(e.target.value);
    }

    render() {
        return(
            <div className='registrationProps'>
                <h1 className='registrationNameProps'>{this.props.name}</h1>
                <input className='registrationProps' value = {this.props.value} onChange={this.handleInputChange}/>
            </div>
        );
    }
}

export default RegistrationInput;