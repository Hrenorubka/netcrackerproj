import React from "react";
import RegistrationInput from "./RegistrationInput";
import './RegistrationStyles.css'
import {MDBBtn} from "mdb-react-ui-kit";


class RegistrationPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          login : '',
          password : '',
          username : '',
          phone : ''
        };

        this.handleChangeLogin = this.handleChangeLogin.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangePhone = this.handleChangePhone.bind(this);
        this.handleRegistrationButtonClick = this.handleRegistrationButtonClick.bind(this);
        this.handleExitButtonClick = this.handleExitButtonClick.bind(this);

    }

    handleChangeLogin(log) {
        this.setState({login: log});
    }

    handleChangePassword(pass) {
        this.setState({password: pass});
    }

    handleChangeName(name) {
        this.setState({username: name});
    }

    handleChangePhone(phone) {
        this.setState({phone: phone});
    }

    handleExitButtonClick() {
        this.props.handleExitButtonClick();
    }

    async handleRegistrationButtonClick() {
        let RegistrationForm = {
            nick : this.state.username,
            password : this.state.password,
            login : this.state.login,
            phone : this.state.phone
        };
        if (this.state.username === '' || this.state.password === '' ||
            this.state.login === '' || this.state.phone === '') {
            alert("You have not entered all information");
            return;
        } else if (this.state.phone.length !== 12) {
            alert("Incorrect phone number");
            return;
        }
        let response;
        try {
            response = await fetch('http://localhost:8080/register', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify(RegistrationForm)
            });
            if (response.status >= 400 && response.status < 500) {
                alert("Incorrect input");
                return;
            }
        } catch (e) {
            alert("Server is not available now");
            return;
        }

    }


    render() {
        return (
            <div className='registrationBigPlace'>
                <MDBBtn outline color='danger' className='RegistrationExit' onClick={this.handleExitButtonClick}><h6>X</h6></MDBBtn>
                <div className='heap'>
                    <h1 className='heap'>Fill the registration form:</h1>
                </div>
                <RegistrationInput name = {'Login:'} value = {this.state.login} handleInputChange = {this.handleChangeLogin}/>
                <RegistrationInput name = {'Password:'} value = {this.state.password} handleInputChange = {this.handleChangePassword}/>
                <RegistrationInput name = {'Your name:'} value = {this.state.username} handleInputChange = {this.handleChangeName}/>
                <RegistrationInput name = {'Your phone:'} value = {this.state.phone} handleInputChange = {this.handleChangePhone}/>
                <MDBBtn outline color='black' className='registrationButton' onClick={this.handleRegistrationButtonClick}><h3>Confirm</h3></MDBBtn>
            </div>
        );
    }

}

export default RegistrationPage;