import React from "react";
import './ProfileStyles.css'

class ProfileHistoryTableServiceColumn extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <li key = {this.props.serviceKey}>
                <p>
                    {this.props.serviceName + ': ' + this.props.serviceCost + '₽'}
                </p>
            </li>
        );
    }
}

export default ProfileHistoryTableServiceColumn;