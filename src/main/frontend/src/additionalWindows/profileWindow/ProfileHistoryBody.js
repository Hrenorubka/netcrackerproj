import React from "react";
import ProfileHistoryTableRow from "./ProfileHistoryTableRow";

class ProfileHistoryBody extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {history} = this.props;
        const elem = (
            history.map(hist =>
                <ProfileHistoryTableRow key = {hist.car.entityId} number = {hist.car.name}
                                        arrivalDate = {hist.arrivalDate} departureDate = {hist.departureDate}
                                        service = {hist.servicePrimitive} parkingPoint = {hist.parkingPointName}/>
            )
        );
        return (<tbody className='ProfileHistory'>{elem}</tbody>);
    }
}

export default ProfileHistoryBody;