import React from "react";
import './ProfileStyles.css'
import ProfileHistoryTable from "./ProfileHistoryTable";
import {MDBBtn} from "mdb-react-ui-kit";

class ProfileWindow extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            userName : '',
            phoneNumber : '',
            history : []
        }

        this.handleBackToMainClick = this.handleBackToMainClick.bind(this);
        this.handleLogOutClick = this.handleLogOutClick.bind(this);
        this._isMounted = false;
    }

    async componentDidMount() {
        this._isMounted = true;
        const responseUserName = await fetch( 'http://localhost:8080/profile/username/' + this.props.login,
            {
                credentials: 'same-origin',
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                }
            });
        if (responseUserName.status >= 200 && responseUserName.status < 300) {
            const bodyUserName = await responseUserName.json();
            if (this._isMounted) {
                this.setState({userName: bodyUserName.username, phoneNumber: bodyUserName.phoneNumber});
            }
        }

        const responceUserHistory = await fetch('http://localhost:8080/profile/history/' + this.props.login,
            {
                credentials: 'same-origin',
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                }
            });
        if (responceUserHistory.status >= 200 && responceUserHistory.status < 300) {
            const bodyUserHistory = await responceUserHistory.json();
            if (this._isMounted) {
                this.setState({history: bodyUserHistory});
            }
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleBackToMainClick() {
        this.props.handleChangePage('Main');
    }

    handleLogOutClick() {
        this.props.handleChangePage('Main');
        this.props.handleChangeLogin('', '', false);
    }


    render() {
        return (
            <div className='ProfileBigPlace'>
                <div className='ProfileHeap'>
                    <h1 className='ProfileHeap'>Your profile</h1>
                    <MDBBtn rounded outline color='black' className='ProfileLogOut' onClick={this.handleLogOutClick}>
                        <h4>Log Out</h4>
                    </MDBBtn>
                    <MDBBtn rounded outline color='black' className='ProfileBackToMain' onClick={this.handleBackToMainClick}>
                        <h4>Back to main page</h4>
                    </MDBBtn>
                </div>
                <div className='ProfileUser'>
                    <h1 className='UserName'>Your name: {this.state.userName}</h1>
                    <h1 className='UserPhone'>Your phone: {this.state.phoneNumber}</h1>
                </div>
                <div className='ProfileHistory'>
                    <h1 className='ProfileHistory'>Booking history:</h1>
                    <ProfileHistoryTable history={this.state.history}/>
                </div>
            </div>
        );
    }
}

export default ProfileWindow;