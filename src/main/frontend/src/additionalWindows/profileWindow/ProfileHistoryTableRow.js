import React from "react";
import './ProfileStyles.css'
import ProfileHistoryTableServiceColumn from "./ProfileHistoryTableServiceColumn";


class ProfileHistoryTableRow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {service} = this.props
        const serviceElem = (
            service.map(serv =>
                <ProfileHistoryTableServiceColumn serviceKey = {serv.id} serviceName = {serv.name}
                                                  serviceCost = {serv.cost}/>
            )
        );
        return (
            <tr className='ProfileHistory' key = {this.props.key}>
                <td className='ProfileHistory'>{this.props.number}</td>
                <td className='ProfileHistory'>{this.props.arrivalDate}</td>
                <td className='ProfileHistory'>{this.props.departureDate}</td>
                <td className='ProfileHistory'>{this.props.parkingPoint}</td>
                <td className='ProfileHistory'>
                    <ul>
                        {serviceElem}
                    </ul>
                </td>
            </tr>
        );
    }
}

export default ProfileHistoryTableRow