import React from "react";
import './ProfileStyles.css'
import ProfileHistoryBody from "./ProfileHistoryBody";


class ProfileHistoryTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <table className='ProfileHistory'>
                <thead>
                    <th className='ProfileHistory'>Car number:</th>
                    <th className='ProfileHistory'>Arrival date:</th>
                    <th className='ProfileHistory'>Departure date:</th>
                    <th className='ProfileHistory'>Parking lot:</th>
                    <th className='ProfileHistory'>Service:</th>
                </thead>

                <ProfileHistoryBody history = {this.props.history}/>

            </table>
        );
    }

}

export default ProfileHistoryTable;