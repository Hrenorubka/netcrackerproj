import React from "react";
import './GetCar.css';
import ServiceRows from "./simpleSections/ServiceRows";
import {MDBBtn} from "mdb-react-ui-kit";

class CarGetSection extends React.Component {

    constructor(props) {
        super(props);
        this.handleButtonGet = this.handleButtonGet.bind(this);
    }

    async handleButtonGet() {
        let carDepDate = {
            carId: this.props.carInfo.car.entityId,
            departureDate: new Date()
        }
        let response = await fetch('http://localhost:8080/car/setDepDate', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(carDepDate)
        });
    }


    render() {
        if (this.props.flag) {
            let costOfAllService = 0;
            this.props.carInfo.carService.map(serv =>
                costOfAllService += serv.cost
            )
            return (
                <div className='carGetSection'>
                    <h2 className='carInfo'>Your car: {this.props.carInfo.car.name}</h2>
                    <h2 className='carInfo'>At: {this.props.carInfo.parkingPoint.name}</h2>
                    <h2 className='carInfo'> Your service:</h2>
                    <table className='serviceInfo'>
                        <thead>
                        <tr>
                            <th>Service:</th>
                            <th>Price:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <ServiceRows service={this.props.carInfo.carService}/>
                        </tbody>
                    </table>
                    <h2 className='FinalPrice'>Final price:</h2>
                    <h2 className='FinalPrice'>{costOfAllService} ₽</h2>
                    <MDBBtn outline color='black' className='getCar' onClick={this.handleButtonGet}><h3>Get car</h3></MDBBtn>
                </div>
            );
        }
        else
            return (
                <div className='carGetSection'/>
            )
    }

}

export default CarGetSection;
