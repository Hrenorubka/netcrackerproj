import React from "react";
import './GetCar.css'
import CarNumberSection from "./CarNumberSection";
import CarGetSection from "./CarGetSection";
import {MDBBtn} from "mdb-react-ui-kit";

class GetCar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            carNumber: '',
            carId: undefined,
            car: undefined,
            flag: false
        }
        this.handleButtonSet = this.handleButtonSet.bind(this);
        this.handleCarNumberChange = this.handleCarNumberChange.bind(this);
        this.handleExitButtonClick = this.handleExitButtonClick.bind(this);
    }

    handleCarNumberChange(e) {
        this.setState({carNumber: e});
    }

    async handleButtonSet() {
        let response;
        try {
            if (this.state.carNumber === '') {
                alert("You have not entered car number");
                return;
            }
            response = await fetch('http://localhost:8080/car/getCarInfo/' + this.state.carNumber);
            if (response.status >= 200 && response.status < 300) {
                try {
                    const body = await response.json();
                    this.setState({car: body});
                    this.setState({flag: true});
                }
                catch (e) {
                    alert("Car not found");
                    return;
                }
            }
            else {
                alert("Car not found");
                return;
            }
        }
        catch (e) {
            alert("Server is not available now");
            return;
        }

    }

    handleExitButtonClick() {
        this.props.handleExitButtonClick();
    }

    render() {
        return(
          <div className='getBigplace'>
            <MDBBtn outline color='danger' className='Exit' onClick={this.handleExitButtonClick}><h6>X</h6></MDBBtn>
            <CarNumberSection
                flag = {this.state.flag} handleButtonSet = {this.handleButtonSet}
                carNumber = {this.state.carNumber} handleCarNumberChange = {this.handleCarNumberChange}
            />
            <CarGetSection flag = {this.state.flag} carInfo = {this.state.car}/>
          </div>
        );
    }
}

export default GetCar;