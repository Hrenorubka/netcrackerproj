import React from "react";
import './GetCar.css';
import {MDBBtn} from "mdb-react-ui-kit";

class CarNumberSection extends React.Component {
    constructor(props) {
        super(props);
        this.handleCarNumberChange = this.handleCarNumberChange.bind(this);
        this.handleButtonSet = this.handleButtonSet.bind(this);
    }

    handleCarNumberChange(e) {
         this.props.handleCarNumberChange(e.target.value);
    }

    handleButtonSet() {
        this.props.handleButtonSet();
    }

    render() {
        return (
            <div className='carNumberSection'>
                <div>
                <h2 className='numberCarText'>Paste your vehicle number:</h2>
                </div>
                <div className='number'>
                    <h1 className='nameNumber'>Vehicle number:</h1>
                    <input className='number' value={this.props.carNumber} onChange={this.handleCarNumberChange}/>
                </div>
                <div className='button'>
                    <MDBBtn outline color='black' className='setNumber' onClick={this.handleButtonSet}><h3>Check</h3></MDBBtn>
                </div>
            </div>
        );
    }
}

export default CarNumberSection;