import React from "react";
import '../GetCar.css';

class ServiceRows extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {service} = this.props;
        const elem = (
            service.map(serv =>
            <tr key = {serv.id}>
                <td>{serv.name}</td>
                <td>{serv.cost + ' ₽'}</td>
            </tr>
            )
        );
        return(
            elem
        );
    }
}

export default ServiceRows;