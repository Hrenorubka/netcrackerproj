import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import GiveCar from './additionalWindows/giveWindow/GiveCar';
import GetCar from "./additionalWindows/getWindow/GetCar";
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import Service from "./additionalWindows/giveWindow/Sections/Service";
import MainPage from "./additionalWindows/mainWindow/MainPage";
import RegistrationPage from "./additionalWindows/registrationWindow/RegistrationPage";
import LoginPage from "./additionalWindows/loginWindow/LoginPage";
import AdminPage from "./additionalWindows/adminWindow/AdminPage";
import ProfileWindow from "./additionalWindows/profileWindow/ProfileWindow";
import PagesChanger from "./PagesChanger";
import 'mdb-react-ui-kit/dist/css/mdb.min.css'

ReactDOM.render(
  <React.StrictMode>
      {/*<App />*/}
      {/*<GiveCar/>*/}
      {/*<Service/>*/}
      {/*<MainPage/>*/}
      {/*<GetCar/>*/}
      {/*<LoginPage/>*/}
      {/*<RegistrationPage/>*/}
      {/*<AdminPage/>*/}
      {/*<ProfileWindow/>*/}
      <PagesChanger/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
