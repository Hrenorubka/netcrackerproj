import React from "react";
import MainPage from "./additionalWindows/mainWindow/MainPage";
import ProfileWindow from "./additionalWindows/profileWindow/ProfileWindow";
import AdminPage from "./additionalWindows/adminWindow/AdminPage";

class  PagesChanger extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            page: 'Main',
            login : '',
            token : '',
            loginFlag : false
        }

        this.handleChangePage = this.handleChangePage.bind(this);
        this.handleChangeLogin = this.handleChangeLogin.bind(this);
    }

    handleChangePage(nextPage) {
        this.setState({page : nextPage});
    }

    handleChangeLogin(login, token, flag) {
        if (login !== undefined && token !== undefined && flag !== undefined)
            this.setState({
                login : login,
                token : token,
                loginFlag : flag
            });
    }

    render() {
        let page = <div>Server is not available now</div>;
        if (this.state.page === 'Main')
            page = <MainPage handleChangeLogin = {this.handleChangeLogin} handleChangePage = {this.handleChangePage}
                             login = {this.state.login} token = {this.state.token} heapFlag = {this.state.loginFlag}/>
        else if (this.state.page === 'Profile')
            page = <ProfileWindow handleChangeLogin = {this.handleChangeLogin} handleChangePage = {this.handleChangePage}
                                  login = {this.state.login} token = {this.state.token}/>
        else if (this.state.page === 'Admin')
            page = <AdminPage login = {this.state.login} token = {this.state.token}
                              handleChangePage = {this.handleChangePage} handleChangeLogOut = {this.handleChangeLogin}/>
        else if (this.state.page === 'Error')
            page = <div><h1>Server is not available now</h1></div>;
        return (
            <div>{page}</div>
        );
    }

}

export default PagesChanger;