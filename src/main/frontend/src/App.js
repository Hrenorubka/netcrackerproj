import React from 'react';
import './App.css';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {entity: [],
        kek: true,
        lol: [] };
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8080/entity/all');
        const body = await response.json();
        console.log(response.data);
        this.setState({entity: body});
    }

    render() {
        const {entity} = this.state;
        return (
            <div>
                <h2>Clients</h2>
                {entity.map(client =>
                    <div>
                        {client.entityId} {client.entityType} {client.name}
                    </div>
                )}
            </div>

        );
    }
}

export default App;
