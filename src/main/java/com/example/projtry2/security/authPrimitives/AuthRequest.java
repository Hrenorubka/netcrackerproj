package com.example.projtry2.security.authPrimitives;

import lombok.Data;

@Data
public class AuthRequest {
    private String login;
    private String password;
}