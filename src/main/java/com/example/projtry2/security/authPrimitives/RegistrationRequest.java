package com.example.projtry2.security.authPrimitives;

import lombok.Data;

@Data
public class RegistrationRequest {

    private String login;
    private String password;
    private String nick;
    private String phone;
}
