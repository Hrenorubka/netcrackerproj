package com.example.projtry2.security;

import com.example.projtry2.entity.User;
import lombok.Builder;
import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;

@Data
@Builder
public class RegistrationForm {

    private String username;
    private String password;
    private String userLogin;
    private String userPhone;

    public User toUser(PasswordEncoder passwordEncoder) {
        return User.builder()
                .username(username)
                .login(userLogin)
                .password(passwordEncoder.encode(password))
                .phoneNumber(userPhone)
                .enabled(true)
                .build();
    }
}
