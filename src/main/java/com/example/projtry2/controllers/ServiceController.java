package com.example.projtry2.controllers;

import com.example.projtry2.repository.AttributeRepository;
import com.example.projtry2.repository.EntityAttributeRepository;
import com.example.projtry2.repository.EntityRepository;
import com.example.projtry2.services.CarService;
import com.example.projtry2.services.CustomerCarService;
import com.example.projtry2.services.primitives.ServicePrimitive;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("service")
@CrossOrigin(origins = "http://localhost:3000/", maxAge = 3600)
public class ServiceController {

    private final CarService carService;

    public ServiceController(EntityAttributeRepository EAVRepository, AttributeRepository AttributeRepository,
                             EntityRepository EntityRepository) {
        carService = new CustomerCarService(EAVRepository, AttributeRepository, EntityRepository);
    }

    @GetMapping("/all")
    public ResponseEntity<List<ServicePrimitive>> getAllCarService() {
        try {
            return new ResponseEntity<>(carService.getAllCarService(), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
