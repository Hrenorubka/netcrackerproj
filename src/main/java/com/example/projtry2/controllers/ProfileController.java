package com.example.projtry2.controllers;

import com.example.projtry2.entity.User;
import com.example.projtry2.repository.JdbcUserRepository;
import com.example.projtry2.repository.RoleUserRepository;
import com.example.projtry2.repository.UserRepository;
import com.example.projtry2.services.Car;
import com.example.projtry2.services.CustomerProfileService;
import com.example.projtry2.services.ProfileService;
import com.example.projtry2.services.primitives.HistoryProfile;
import com.example.projtry2.services.primitives.UserProfileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("profile")
@CrossOrigin(origins = "http://localhost:3000/", maxAge = 3600)
@PreAuthorize("hasRole('USER')")
public class ProfileController {

    private final UserRepository userRepository;
    private final ProfileService profileService;

    @Autowired
    public ProfileController(UserRepository userRepository, Car car){
        this.userRepository = userRepository;
        this.profileService = new CustomerProfileService(userRepository, car);
    }

    @GetMapping("/username/{userLogin}")
    public ResponseEntity<UserProfileInfo> getUserName(@PathVariable String userLogin) {
        try {
            User user = userRepository.findByLogin(userLogin);
            UserProfileInfo out = UserProfileInfo.builder()
                    .userLogin(userLogin)
                    .phoneNumber(user.getPhoneNumber())
                    .username(user.getUsername())
                    .build();
            return new ResponseEntity<>(out, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/history/{userLogin}")
    public ResponseEntity<List<HistoryProfile>> getUserHistory (@PathVariable String userLogin) {
        try {
            return new ResponseEntity<>(profileService.getHistoryProfile(userLogin), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }


}
