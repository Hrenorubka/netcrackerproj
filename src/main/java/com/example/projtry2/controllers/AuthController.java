package com.example.projtry2.controllers;


import com.example.projtry2.entity.User;
import com.example.projtry2.security.authPrimitives.AuthRequest;
import com.example.projtry2.security.authPrimitives.AuthResponse;
import com.example.projtry2.security.authPrimitives.RegistrationRequest;
import com.example.projtry2.security.jwt.JwtProvider;
import com.example.projtry2.services.UserService;
import com.example.projtry2.services.primitives.UserProfileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;


@RestController
@RequestMapping()
@CrossOrigin(origins = "http://localhost:3000/", maxAge = 3600)
public class AuthController {
    @Autowired
    private UserService userService;
    @Autowired
    private JwtProvider jwtProvider;

    @PostMapping("/register")
    public String registerUser(@RequestBody RegistrationRequest registrationRequest) {
        User u = User.builder().build();
        u.setPassword(registrationRequest.getPassword());
        u.setLogin(registrationRequest.getLogin());
        u.setUsername(registrationRequest.getNick());
        u.setEnabled(true);
        userService.saveUser(u);
        return "OK";
    }

    @PostMapping("/auth")
    public ResponseEntity<AuthRequest> auth(@RequestBody AuthRequest request) {
        try {
            User userEntity = userService.findByLoginAndPassword(request.getLogin(), request.getPassword());
            String token = jwtProvider.generateToken(userEntity.getLogin());
            return new ResponseEntity(new AuthResponse(token), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/admin/check")
    public ResponseEntity<String> adminCheck(@RequestBody AuthRequest request) {
        try {
            User userEntity = userService.findByLoginAndPassword(request.getLogin(), request.getPassword());
            if (userEntity.getRole().getName().equals("ROLE_ADMIN"))
                return new ResponseEntity<>("Ok", HttpStatus.OK);
            return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
        }
    }
}