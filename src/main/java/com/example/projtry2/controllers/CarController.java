package com.example.projtry2.controllers;


import com.example.projtry2.repository.AttributeRepository;
import com.example.projtry2.repository.RoleUserRepository;
import com.example.projtry2.services.*;
import com.example.projtry2.services.primitives.AdminCarInfo;
import com.example.projtry2.services.primitives.CarGetPrimitive;
import com.example.projtry2.services.primitives.CarInfo;
import com.example.projtry2.repository.EntityAttributeRepository;
import com.example.projtry2.repository.EntityRepository;
import com.example.projtry2.services.primitives.DepartureDatePrimitive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("car")
@CrossOrigin(origins = "http://localhost:3000/", maxAge = 3600)
public class CarController {


    private final Car car;
    private final ParkingPoint parkingPoint;
    private final AdminService adminService;

    @Autowired
    CarController(EntityRepository entityRepository, EntityAttributeRepository entityAttributeRepository,
                  JdbcTemplate jdbcTemplate, RoleUserRepository roleUserRepository, AttributeRepository attributeRepository) {
        car = new CustomerCar(entityRepository, entityAttributeRepository, jdbcTemplate, roleUserRepository);
        parkingPoint =  new CustomerParkingPoint(entityAttributeRepository, entityRepository);
        adminService = new CustomerAdminService(entityRepository, entityAttributeRepository,
                jdbcTemplate, roleUserRepository, attributeRepository);
    }

    @PostMapping
    public ResponseEntity createCar(@RequestBody CarInfo carInfo) throws URISyntaxException {
        if (carInfo == null || carInfo.getCarType() == null || carInfo.getInPlace() == null ||
                carInfo.getListServicesId() == null || carInfo.getParkingPoint() == null ||
                carInfo.getDateEvent() == null || carInfo.getCarNumber() == null)
        {
            return new ResponseEntity("Null segments", HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }
        try {
            Long carId = car.setCar(carInfo);
            parkingPoint.setCarToParkingPoint(
                    parkingPoint.getParkingPointByName(carInfo.getParkingPoint()).getEntityId(), carId
            );
            return ResponseEntity.created(new URI("/car/" + carId)).body(carId);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/setDepDate")
    public ResponseEntity setDepartureDate(@RequestBody DepartureDatePrimitive departureDate) throws URISyntaxException {
        if (!car.carHere(departureDate.getCarId())) {
            return ResponseEntity.badRequest().body("Car already departure");
        }
        Long carId = car.setDepartureDate(departureDate);
        parkingPoint.removeCarFromParkingPoint(parkingPoint.getParkingPointByCarId(carId).getEntityId(), carId);
        return ResponseEntity.accepted().body(carId);
    }

    @GetMapping("/service/{carId}")
    public ResponseEntity<Map<String, Integer>> getServiceOfCar(@PathVariable Long carId) {
        try {
            return new ResponseEntity<>(car.getServiceOfCar(carId), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/getCarInfo/{number}")
    public ResponseEntity<CarGetPrimitive> getCarInfo(@PathVariable String number) {
            try {
                return new ResponseEntity<>(car.getCarInfo(number), HttpStatus.OK);
            }
            catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            }
        }


    @GetMapping("/admin/getAllCars")
    public ResponseEntity<List<AdminCarInfo>> getAllCars() {
        try {
            return new ResponseEntity<>(adminService.getAllCars(), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }


}
