package com.example.projtry2.controllers;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@PreAuthorize("hasRole('ADMIN')")
public class TestController {

    @GetMapping("/")
    public String test() {
        return "test";
    }
}
