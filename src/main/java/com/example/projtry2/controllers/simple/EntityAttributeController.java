package com.example.projtry2.controllers.simple;


import com.example.projtry2.entity.Value;
import com.example.projtry2.repository.EntityAttributeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("eav")
@CrossOrigin(origins = "http://localhost:3000/", maxAge = 3600)
@PreAuthorize("hasRole('ADMIN')")
public class EntityAttributeController {


    private final EntityAttributeRepository entityAttributeRepository;

    @Autowired
    public EntityAttributeController(EntityAttributeRepository entityAttributeRepository) {
        this.entityAttributeRepository = entityAttributeRepository;
    }

    @GetMapping("/getAll/{entityId}")
    public ResponseEntity<List<Value>> getEntityValuesOfAttributes(@PathVariable Long entityId) {
        return new ResponseEntity<>(entityAttributeRepository.getEntityValuesOfAttributes(entityId), HttpStatus.OK);
    }

    @GetMapping("/setEntityValuesOfAttributes")
    public String setEntityValuesOfAttributes() {
        entityAttributeRepository.setEntityValueOfAttribute(4L, 5L, 300);
        return "xe xe";
    }

    @GetMapping("/nested/{id}")
    public List<Long> getNested(@PathVariable Long id) {
        return entityAttributeRepository.getEntityNestedEntitiesId(id);
    }

    @GetMapping("/{entityid}/{atrrid}")
    public Value getValue(@PathVariable Long entityid, @PathVariable Long atrrid) {
        return entityAttributeRepository.getEntityValueOfAttribute(entityid, atrrid);
    }

}
