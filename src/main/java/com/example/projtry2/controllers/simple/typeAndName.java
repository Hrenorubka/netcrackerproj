package com.example.projtry2.controllers.simple;

import lombok.Data;

@Data
class typeAndName {
    private Long typeId;
    private String name;

    public typeAndName(Long typeId, String name) {
        this.typeId = typeId;
        this.name = name;
    }
}
