package com.example.projtry2.controllers.simple;

import com.example.projtry2.entity.Attribute;

import com.example.projtry2.repository.AttributeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("attribute")
@CrossOrigin(origins = "http://localhost:3000/", maxAge = 3600)
@PreAuthorize("hasRole('ADMIN')")
public class AttributeController {


    private final AttributeRepository attributeRepository;

    @Autowired
    public AttributeController(AttributeRepository attributeRepository) {
        this.attributeRepository = attributeRepository;
    }

    @GetMapping("/{attributeId}")
    public ResponseEntity<Attribute> getAttribute(@PathVariable Long attributeId) {
        return new ResponseEntity<>(attributeRepository.getAttributeById(attributeId), HttpStatus.OK);

    }

}
