package com.example.projtry2.controllers.simple;

import com.example.projtry2.controllers.simple.typeAndName;
import com.example.projtry2.entity.Entity;
import com.example.projtry2.repository.EntityRepository;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000/", maxAge = 3600)
@RestController
@RequestMapping("entity")
@PreAuthorize("hasRole('ADMIN')")
public class EntityController {

    private final EntityRepository entityRepos;

    @Autowired
    public EntityController(EntityRepository entityRepos) {
        this.entityRepos = entityRepos;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Entity> getEntity(@PathVariable Long id) {
        return new ResponseEntity<>(entityRepos.getEntityById(id), HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Entity>> getAllEntities() {
        return new ResponseEntity<>(entityRepos.getAllEntity(), HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<List<Entity>> getEntityByName(@PathVariable String name) {
        return new ResponseEntity<>(entityRepos.getEntitiesByName(name), HttpStatus.OK);
    }

    @GetMapping("/{entityType}/{name}")
    public String setEntity(@PathVariable Long entityType, @PathVariable String name) {
        Long key = entityRepos.addEntity(entityType, name);
        return key.toString();
    }

    @PostMapping(path = "/set")
    public void psetEntity(@RequestBody typeAndName inp) { // @RequestBody
        entityRepos.addEntity(inp.getTypeId(), inp.getName());
    }

    @GetMapping("/del/{entityId}")
    public String delEntity(@PathVariable Long entityId) {
        entityRepos.deleteEntityById(entityId);
        return "deleted";
    }

    @DeleteMapping(value = "/pdelEntity", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void pdelEntity(@RequestBody Long entityId) {
        entityRepos.deleteEntityById(entityId);
    }

    @GetMapping("/updateEntity/{entityId}/{entityType}/{name}")
    public String updateEntity(@PathVariable Long entityId, @PathVariable Long entityType,
                               @PathVariable String name) {
        Entity testEnt = Entity.builder()
                .entityId(entityId)
                .entityType(entityType)
                .name(name)
                .build();
        entityRepos.updateEntity(entityId, testEnt);
        return "Updated";
    }

    @PutMapping("/pupdateEntity")
    public void pupdateEntity(@RequestBody Entity updatedEntity) {
        entityRepos.updateEntity(updatedEntity.getEntityId(), updatedEntity);
    }
}
