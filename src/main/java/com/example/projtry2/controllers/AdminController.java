package com.example.projtry2.controllers;

import com.example.projtry2.services.AdminService;
import com.example.projtry2.services.primitives.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("admin")
@PreAuthorize("hasRole('ADMIN')")
public class AdminController {

    private final AdminService adminService;

    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping("/getAllCarsInfo")
    public ResponseEntity<List<AdminCarInfo>> getAllCarsInfo() {
        return new ResponseEntity<>(adminService.getAllCars(), HttpStatus.OK);
    }

    @PostMapping("/updateCarInfo")
    public ResponseEntity<String> updateCarInfo(@RequestBody AdminUpdateCarInfo adminUpdateCarInfo) {
        try {
            adminService.updateCarInfo(adminUpdateCarInfo);
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/deleteCar/{carId}")
    public ResponseEntity deleteCar(@PathVariable Long carId) {
        try {
            adminService.deleteCar(carId);
            System.out.println("kek");
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/updateService/cost")
    public ResponseEntity updateService(@RequestBody ServicePrimitive servicePrimitive) {
        try {
            adminService.updateCostOfService(servicePrimitive);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity("", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/updateService/delete/{id}")
    public ResponseEntity deleteService(@PathVariable Long id) {
        try {
            adminService.deleteService(id);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("/updateService/create")
    public ResponseEntity createService(@RequestBody ServicePrimitive servicePrimitive) {
        try {
            adminService.createNewService(servicePrimitive);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/allService")
    public ResponseEntity<List<ServicePrimitive>> getAllService() {
        return new ResponseEntity<>(adminService.getAllService(), HttpStatus.OK);
    }

    @PostMapping("/updateParkingPoint/name")
    public ResponseEntity updateParkingPoint(@RequestBody ParkingPointPrimitive parkingPointPrimitive) {
        try {
            adminService.updateParkingPointName(parkingPointPrimitive);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/updateParkingPoint/cargo")
    public ResponseEntity updateCargoParkingPoint(@RequestBody ParkingPointPrimitive parkingPointPrimitive) {
        try {
            adminService.updateParkingPointCargo(parkingPointPrimitive);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/updateParkingPoint/passenger")
    public ResponseEntity updatePassengerParkingPoint(@RequestBody ParkingPointPrimitive parkingPointPrimitive) {
        try {
            adminService.updateParkingPointPassenger(parkingPointPrimitive);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/updateParkingPoint/delete/{id}")
    public ResponseEntity deleteParkingPoint(@PathVariable Long id) {
        try {
            adminService.deleteParkingPoint(id);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (IllegalCallerException exception) {
            return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
        }
        catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/updateParkingPoint/create")
    public ResponseEntity createParkingPoint (@RequestBody ParkingPointPrimitive parkingPointPrimitive) {
        if (parkingPointPrimitive == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        try {
            adminService.createParkingPoint(parkingPointPrimitive);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/allParkingPoints")
    public ResponseEntity<List<ParkingPointPrimitive>> getAllParkingPoints () {
        return new ResponseEntity<>(adminService.getAllParkingPoints(), HttpStatus.OK);
    }

}
