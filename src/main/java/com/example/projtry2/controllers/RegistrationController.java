package com.example.projtry2.controllers;


import com.example.projtry2.repository.UserRepository;
import com.example.projtry2.security.RegistrationForm;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("register")
@CrossOrigin(origins = "http://localhost:3000/", maxAge = 3600)
public class RegistrationController {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public RegistrationController(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    public String registerForm() {
        return "registration";
    }

    @PostMapping("/r")
    public ResponseEntity<String> processRegistration(@RequestBody RegistrationForm form) throws URISyntaxException {
        try {
            userRepository.save(form.toUser(passwordEncoder));
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
