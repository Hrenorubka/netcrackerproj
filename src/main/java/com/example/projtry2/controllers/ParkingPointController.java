package com.example.projtry2.controllers;


import com.example.projtry2.entity.Entity;
import com.example.projtry2.repository.EntityAttributeRepository;
import com.example.projtry2.repository.EntityRepository;
import com.example.projtry2.services.CustomerCar;
import com.example.projtry2.services.CustomerParkingPoint;
import com.example.projtry2.services.ParkingPoint;
import com.example.projtry2.services.primitives.ParkingPointPrimitive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("parkingPoint")
@CrossOrigin(origins = "http://localhost:3000/", maxAge = 3600)
public class ParkingPointController {

    private final ParkingPoint parkingPoint;

    @Autowired
    ParkingPointController(EntityRepository entityRepository, EntityAttributeRepository entityAttributeRepository) {
        parkingPoint = new CustomerParkingPoint(entityAttributeRepository, entityRepository);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Entity>> getAllParkingPoints() {
        try {
            return new ResponseEntity<>(parkingPoint.giveAllParkingPoints(), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/all/info")
    public ResponseEntity<List<ParkingPointPrimitive>> getAllParkingPointPrimitives() {
        try {
            List<ParkingPointPrimitive> out = new ArrayList<>();
            List<Entity> allParkingPoints = parkingPoint.giveAllParkingPoints();
            for (Entity pP : allParkingPoints) {
                out.add(parkingPoint.giveParkingPointInfo(pP.getEntityId()));
            }
            return new ResponseEntity<>(out, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}/info")
    public ResponseEntity<ParkingPointPrimitive> getParkingPointPrimitive(@PathVariable Long id) {
        try {
            return new ResponseEntity<>(parkingPoint.giveParkingPointInfo(id), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
