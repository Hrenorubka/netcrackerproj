package com.example.projtry2.services;

import com.example.projtry2.entity.Entity;
import com.example.projtry2.services.primitives.ParkingPointPrimitive;

import java.util.List;

public interface ParkingPoint {

    public void setCarToParkingPoint(Long parkingPointId, Long carId);

    public void removeCarFromParkingPoint(Long parkingPointId, Long carId);

    public void deleteCarFromParkingPoint(Long parkingPointId, Long carId);

    public Integer giveNumberOfCargoSeats(Long parkingPointId);

    public Integer giveNumberOfPassengerSeats(Long parkingPointId);

    public Integer giveNumberOfFreeCargoSeats(Long parkingPointId);

    public Integer giveNumberOfFreePassengerSeats(Long parkingPointId);

    public Entity getParkingPointByName(String name);

    public Entity getParkingPointByCarId(Long carId);

    public ParkingPointPrimitive giveParkingPointInfo(Long parkingPointId);

    public List<Entity> giveAllParkingPoints();

    public void updateCarParkingPoint(Long carId, Long parkingPointId);

    public void updateParkingPointName(Long parkingPointId, String name);

    public void updateParkingPointPassenger(Long parkingPointId, Integer numPassenger);

    public void updateParkingPointCargo(Long parkingPointId, Integer numCargo);

    public void deleteParkingPoint(Long parkingPointId);

    public void createParkingPoint(ParkingPointPrimitive parkingPointPrimitive);

}
