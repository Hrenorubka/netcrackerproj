package com.example.projtry2.services;

import com.example.projtry2.services.primitives.HistoryProfile;

import java.util.List;

public interface ProfileService {

    public List<HistoryProfile> getHistoryProfile(String userLogin);

}
