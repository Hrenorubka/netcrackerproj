package com.example.projtry2.services;

import com.example.projtry2.entity.Entity;
import com.example.projtry2.services.primitives.AdminCarInfo;
import com.example.projtry2.services.primitives.AdminUpdateCarInfo;
import com.example.projtry2.services.primitives.ParkingPointPrimitive;
import com.example.projtry2.services.primitives.ServicePrimitive;

import java.util.List;

public interface AdminService {


    public List<AdminCarInfo> getAllCars();

    public void updateCarInfo(AdminUpdateCarInfo adminUpdateCarInfo);

    public void deleteCar(Long carId);

    public List<ServicePrimitive> getAllService();

    public void updateCostOfService(ServicePrimitive servicePrimitive);

    public void createNewService(ServicePrimitive servicePrimitive);

    public void deleteService(Long serviceId);

    public List<ParkingPointPrimitive> getAllParkingPoints();

    public void updateParkingPointName(ParkingPointPrimitive parkingPointPrimitive);

    public void updateParkingPointPassenger(ParkingPointPrimitive parkingPointPrimitive);

    public void updateParkingPointCargo(ParkingPointPrimitive parkingPointPrimitive);

    public void deleteParkingPoint(Long parkingPointId);

    public void createParkingPoint(ParkingPointPrimitive parkingPointPrimitive);
}
