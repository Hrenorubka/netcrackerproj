package com.example.projtry2.services;

import com.example.projtry2.entity.Entity;
import com.example.projtry2.entity.User;
import com.example.projtry2.repository.JdbcEntityRepository;
import com.example.projtry2.services.primitives.CarGetPrimitive;
import com.example.projtry2.services.primitives.CarInfo;
import com.example.projtry2.services.primitives.DepartureDatePrimitive;
import com.example.projtry2.services.primitives.ServicePrimitive;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface Car {

    public Long setDepartureDate(DepartureDatePrimitive datePrimitive);

    public Long setCar(CarInfo carInfo);

    public Map<String, Integer> getServiceOfCar(Long carId);

    public List<ServicePrimitive> getServicePrimitiveOfCar(Long carId);

    public CarGetPrimitive getCarInfo(String carNumber);

    public Entity getCarById(Long carId);

    public User getCarUser(Long carId);

    public Entity getCarParkingPoint(Long carId);

    public List<Entity> getAllCar();

    public String getCarDepartureDate(Long carId);

    public String getCarArrivalDate(Long carId);

    public Boolean carHere(Long carId);

    public String setCarToNewUser(Long carId, Long userId);

    public void deleteCar(Long carId);

}
