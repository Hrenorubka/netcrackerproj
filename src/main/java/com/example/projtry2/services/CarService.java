package com.example.projtry2.services;

import com.example.projtry2.entity.Entity;
import com.example.projtry2.services.primitives.ServicePrimitive;

import java.util.List;
import java.util.Map;

public interface CarService {


    public Integer getCostOfService(Long serviceId);

    public Entity getServiceByName(String name);

    public List<ServicePrimitive> getAllCarService();

    public void updateCostOfService(Long serviceId, Integer newCost);

    public Entity createNewService(ServicePrimitive servicePrimitive);

    public void deleteServiceById(Long serviceId);

}
