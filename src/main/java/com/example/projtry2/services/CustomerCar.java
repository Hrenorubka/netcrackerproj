package com.example.projtry2.services;

import com.example.projtry2.entity.Entity;
import com.example.projtry2.entity.User;
import com.example.projtry2.repository.*;
import com.example.projtry2.services.primitives.CarGetPrimitive;
import com.example.projtry2.services.primitives.CarInfo;
import com.example.projtry2.services.primitives.DepartureDatePrimitive;
import com.example.projtry2.services.primitives.ServicePrimitive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CustomerCar implements Car {

    private Long carId;
    private EntityAttributeRepository EAV;
    private EntityRepository entityRepository;
    private UserRepository userRepository;

    @Autowired
    public CustomerCar(EntityRepository entityRepository, EntityAttributeRepository EAV,
                       JdbcTemplate jdbcTemplate, RoleUserRepository roleUserRepository) {
        this.entityRepository = entityRepository;
        this.EAV = EAV;
        this.userRepository = new JdbcUserRepository(jdbcTemplate, roleUserRepository);
    }


    @Override
    public Long setCar(CarInfo carInfo) {
        Long carId = entityRepository.addEntity(2L, carInfo.getCarNumber());
        EAV.setEntityValueOfAttribute(carId, 2L, carInfo.getDateEvent()); // AttributeId = 2 -> ArrivalDate
        EAV.setEntityValueOfAttribute(carId, 12L, carInfo.getInPlace()); // 12 -> inPlace (Car here?)
        EAV.setEntityValueOfAttribute(carId, 1L, carInfo.getCarType()); // 1 -> TypeCar
        for (Long a : carInfo.getListServicesId()) {
            EAV.setEntityValueOfAttribute(carId, 6L, a); // 6 -> ReffToServiceEntity
        }
        Long userId;
        try {
            userId = userRepository.findByPhone(carInfo.getPhoneNumber()).getId();
        }
        catch (Exception e) {
            userId = userRepository.setGuestByPhoneNumber(carInfo.getPhoneNumber());
        }
        userRepository.setCarToUser(userId, carId);
        return carId;
    }

    @Override
    public Long setDepartureDate(DepartureDatePrimitive datePrimitive) {
        if (entityRepository.getEntityById(datePrimitive.getCarId()).getEntityType() != 2L) {
            throw new IllegalArgumentException();
        }
        EAV.setEntityValueOfAttribute(datePrimitive.getCarId(), 10L, datePrimitive.getDepartureDate()); // 10 -> DepartureDate
        EAV.updateEntityValueOfAttribute(datePrimitive.getCarId(), 12L, false); // 12 -> InPlace
        return datePrimitive.getCarId();
    }



    @Override
    public Map<String, Integer> getServiceOfCar(Long carId) {
        if (entityRepository.getEntityById(carId).getEntityType() != 2L) {
            throw new IllegalArgumentException();
        }
        List<Long> servicesId = EAV.getEntityNestedEntitiesId(carId); // 6 -> ReffToServiceEntity
        if (servicesId == null) {
            return null;
        }
        Map<String, Integer> out = new HashMap<>();
        for (Long a: servicesId) {
            String nameService = entityRepository.getEntityById(a).getName();
            Integer costService = EAV.getEntityValueOfAttribute(a, 5L).getIntValue(); // 5-> costEntity
            out.put(nameService, costService);
        }
        return out;
    }

    @Override
    public List<ServicePrimitive> getServicePrimitiveOfCar(Long carId) {
        if (entityRepository.getEntityById(carId).getEntityType() != 2L) {
            throw new IllegalArgumentException();
        }
        List<Long> servicesId = EAV.getEntityNestedEntitiesId(carId); // 6 -> ReffToServiceEntity
        if (servicesId == null) {
            return null;
        }
        List<ServicePrimitive> servicePrimitives = new ArrayList<>();
        for (Long a: servicesId) {
            String nameService = entityRepository.getEntityById(a).getName();
            Integer costService = EAV.getEntityValueOfAttribute(a, 5L).getIntValue(); // 5-> costEntity
            servicePrimitives.add(
                    ServicePrimitive.builder()
                            .id(a)
                            .cost(costService)
                            .name(nameService)
                            .build());
        }
        return servicePrimitives;
    }

    @Override
    public CarGetPrimitive getCarInfo(String carNumber) {
        List<Entity> cars = entityRepository.getEntitiesByName(carNumber);
        Entity car = null;
        for (Entity a : cars) {
            if (entityRepository.getEntityById(a.getEntityId()).getEntityType() != 2L) {
                throw new IllegalArgumentException();
            }
            if (EAV.getEntityValueOfAttribute(a.getEntityId(), 12L).getBoolValue())
            {
                car = a;
            }
        }
        if (car == null) {
            return null;
        }
        List<Long> servicesId = EAV.getEntityNestedEntitiesId(car.getEntityId()); // 6 -> ReffToServiceEntity
        List<ServicePrimitive> carService = new ArrayList<>();
        for (Long a: servicesId) {
            ServicePrimitive curService = ServicePrimitive.builder()
                    .id(a)
                    .flag(true)
                    .name(entityRepository.getEntityById(a).getName())
                    .cost(EAV.getEntityValueOfAttribute(a, 5L).getIntValue())
                    .build();
            carService.add(curService);
        }
        Long parkingPointId = EAV.getEntityParentsId(car.getEntityId()).get(0);
        return CarGetPrimitive.builder()
                .car(car)
                .carService(carService)
                .parkingPoint(entityRepository.getEntityById(parkingPointId))
                .build();
    }

    @Override
    public Entity getCarById(Long carId) {
        if (entityRepository.getEntityById(carId).getEntityType() != 2L) {
            throw new IllegalArgumentException();
        }
        return entityRepository.getEntityById(carId);
    }

    @Override
    public User getCarUser(Long carId) {
        if (entityRepository.getEntityById(carId).getEntityType() != 2L) {
            throw new IllegalArgumentException();
        }
        return userRepository.getUserByCarId(carId);
    }

    @Override
    public Entity getCarParkingPoint(Long carId) {
        if (entityRepository.getEntityById(carId).getEntityType() != 2L) {
            throw new IllegalArgumentException();
        }
        Long parkingPointId = null;
        try {
             parkingPointId = EAV.getEntityParentsId(carId).get(0);
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return entityRepository.getEntityById(parkingPointId);
    }

    @Override
    public List<Entity> getAllCar() {
        return entityRepository.getEntityByTypeId(2L);
    }

    @Override
    public String getCarDepartureDate(Long carId) {
        try {
            if (entityRepository.getEntityById(carId).getEntityType() != 2L) {
                throw new IllegalArgumentException();
            }
            return EAV.getEntityValueOfAttribute(carId, 10L).getDateValue();
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String getCarArrivalDate(Long carId) {
        try {
            if (entityRepository.getEntityById(carId).getEntityType() != 2L) {
                throw new IllegalArgumentException();
            }
            return EAV.getEntityValueOfAttribute(carId, 2L).getDateValue();
        }
        catch (Exception e) {
            throw new IllegalArgumentException();

        }
    }

    @Override
    public Boolean carHere(Long carId) {
        try {
            if (entityRepository.getEntityById(carId).getEntityType() != 2L) {
                throw new IllegalArgumentException();
            }
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return EAV.getEntityValueOfAttribute(carId, 12L).getBoolValue();
    }

    @Override
    public String setCarToNewUser(Long carId, Long userId) {
        try {
            userRepository.getUserById(userId);
            if (entityRepository.getEntityById(carId).getEntityType() != 2L) {
                throw new IllegalArgumentException();
            }
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
        userRepository.removeUserCar(userRepository.getUserByCarId(carId).getId(), carId);
        userRepository.setCarToUser(userId, carId);
        return "Ok";
    }

    @Override
    public void deleteCar(Long carId) {
        try {
            if (entityRepository.getEntityById(carId).getEntityType() != 2L) {
                throw new IllegalArgumentException();
            }
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
        entityRepository.deleteEntityById(carId);
    }


}
