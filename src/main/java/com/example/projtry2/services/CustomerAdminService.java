package com.example.projtry2.services;

import com.example.projtry2.entity.Entity;
import com.example.projtry2.entity.User;
import com.example.projtry2.repository.*;
import com.example.projtry2.services.primitives.AdminCarInfo;
import com.example.projtry2.services.primitives.AdminUpdateCarInfo;
import com.example.projtry2.services.primitives.ParkingPointPrimitive;
import com.example.projtry2.services.primitives.ServicePrimitive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.AccessType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class CustomerAdminService implements AdminService{

    private UserRepository userRepository;
    private CarService service;
    private Car carService;
    private ParkingPoint parkingPoint;


    @Autowired
    public CustomerAdminService (EntityRepository entityRepository, EntityAttributeRepository EAV,
                                 JdbcTemplate jdbcTemplate, RoleUserRepository roleUserRepository,
                                 AttributeRepository attributeRepository) {
        carService = new CustomerCar(entityRepository, EAV, jdbcTemplate, roleUserRepository);
        parkingPoint = new CustomerParkingPoint(EAV, entityRepository);
        userRepository = new JdbcUserRepository(jdbcTemplate, roleUserRepository);
        service = new CustomerCarService(EAV, attributeRepository, entityRepository);
    }

    @Override
    public List<AdminCarInfo> getAllCars() {

        // get All Car Entity
        // get Their Parking Point
        // get User of Car, where car have user

        List<Entity> allCars = carService.getAllCar();
        List<AdminCarInfo> resultSet = new ArrayList<>();

        for (Entity a : allCars) {
            if (carService.carHere(a.getEntityId())) {
                Entity curParkingPoint = carService.getCarParkingPoint(a.getEntityId());
                User curUser;
                try {
                    curUser = carService.getCarUser(a.getEntityId());
                }
                catch (Exception e) {
                    curUser = null;
                }
                String arrivalDate = carService.getCarArrivalDate(a.getEntityId());
                String departureDate;
                try {
                    departureDate = carService.getCarDepartureDate(a.getEntityId());
                }
                catch (Exception e) {
                    departureDate = null;
                }
                resultSet.add(
                        AdminCarInfo.builder()
                                .car(a)
                                .parkingPoint(curParkingPoint)
                                .user(curUser)
                                .arrivalDate(arrivalDate)
                                .depDate(departureDate)
                                .build()
                );
            }
        }
        return resultSet;
    }

    @Override
    public void updateCarInfo(AdminUpdateCarInfo adminUpdateCarInfo) {
        try {
            Long parkingPointId = parkingPoint.getParkingPointByName(adminUpdateCarInfo.getParkingPoint()).getEntityId();
            parkingPoint.deleteCarFromParkingPoint(
                    parkingPoint.getParkingPointByCarId(adminUpdateCarInfo.getCarId()).getEntityId(),
                    adminUpdateCarInfo.getCarId());
            parkingPoint.setCarToParkingPoint(parkingPointId, adminUpdateCarInfo.getCarId());
            Long userId = userRepository.findByPhone(adminUpdateCarInfo.getOwnerPhone()).getId();
            carService.setCarToNewUser(adminUpdateCarInfo.getCarId(), userId);
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void deleteCar(Long carId) {
        try {
            parkingPoint.getParkingPointByCarId(carId);
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
        parkingPoint.removeCarFromParkingPoint(parkingPoint.getParkingPointByCarId(carId).getEntityId(), carId);
        carService.deleteCar(carId);
    }

    @Override
    public List<ServicePrimitive> getAllService() {
        return service.getAllCarService();
    }

    @Override
    public void updateCostOfService(ServicePrimitive servicePrimitive) {
        if (servicePrimitive.getCost() <= 0)
            throw new IllegalArgumentException();
        service.updateCostOfService(servicePrimitive.getId(), servicePrimitive.getCost());
    }

    @Override
    public void createNewService(ServicePrimitive servicePrimitive) {
        service.createNewService(servicePrimitive);
    }

    @Override
    public void deleteService(Long serviceId) {
        service.deleteServiceById(serviceId);
    }

    @Override
    public List<ParkingPointPrimitive> getAllParkingPoints() {
        List<Entity> allParkingPoints = parkingPoint.giveAllParkingPoints();
        List<ParkingPointPrimitive> out = new ArrayList<>();
        for (Entity a : allParkingPoints) {
            out.add(parkingPoint.giveParkingPointInfo(a.getEntityId()));
        }
        return out;
    }

    @Override
    public void updateParkingPointName(ParkingPointPrimitive parkingPointPrimitive) {
        try {
            parkingPoint.updateParkingPointName(parkingPointPrimitive.getParkingPoint().getEntityId(),
                    parkingPointPrimitive.getParkingPoint().getName());
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void updateParkingPointPassenger(ParkingPointPrimitive parkingPointPrimitive) {
        parkingPoint.updateParkingPointPassenger(parkingPointPrimitive.getParkingPoint().getEntityId(),
                parkingPointPrimitive.getNumberOfPassengerAll());
    }

    @Override
    public void updateParkingPointCargo(ParkingPointPrimitive parkingPointPrimitive) {
        parkingPoint.updateParkingPointCargo(parkingPointPrimitive.getParkingPoint().getEntityId(),
                parkingPointPrimitive.getNumberOfCargoAll());
    }

    @Override
    public void deleteParkingPoint(Long parkingPointId) {
        parkingPoint.deleteParkingPoint(parkingPointId);
    }

    @Override
    public void createParkingPoint(ParkingPointPrimitive parkingPointPrimitive) {
        parkingPoint.createParkingPoint(parkingPointPrimitive);
    }
}
