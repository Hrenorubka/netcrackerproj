package com.example.projtry2.services;

import com.example.projtry2.entity.Entity;
import com.example.projtry2.repository.EntityAttributeRepository;
import com.example.projtry2.repository.EntityRepository;
import com.example.projtry2.services.primitives.ParkingPointPrimitive;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CustomerParkingPoint implements ParkingPoint{

    private EntityAttributeRepository EAVRepository;
    private com.example.projtry2.repository.EntityRepository EntityRepository;

    @Autowired
    public CustomerParkingPoint(EntityAttributeRepository EAVRepository, EntityRepository EntityRepository) {
        this.EntityRepository = EntityRepository;
        this.EAVRepository = EAVRepository;
    }

    @Override
    public void setCarToParkingPoint(Long parkingPointId, Long carId) {
        try {
            if (EntityRepository.getEntityById(carId).getEntityType() != 2L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        EAVRepository.setEntityValueOfAttribute(parkingPointId, 6L, carId);
        String typeCar = EAVRepository.getEntityValueOfAttribute(carId, 1L).getStringValue();
        if (typeCar.equals("Passenger")) {
            Integer count = EAVRepository.getEntityValueOfAttribute(parkingPointId, 14L).getIntValue();
            try {
                if (count == 0)
                    throw new RuntimeException();
            } catch (Exception e) {
                System.out.println("Count cant be < 0");
            };
            EAVRepository.updateEntityValueOfAttribute(parkingPointId, 14L, count - 1);
        } else if (typeCar.equals("Cargo")) {
            Integer count = EAVRepository.getEntityValueOfAttribute(parkingPointId, 13L).getIntValue();
            try {
                if (count == 0)
                    throw new RuntimeException();
            } catch (Exception e) {
                System.out.println("Count cant be < 0");
            };
            EAVRepository.updateEntityValueOfAttribute(parkingPointId, 13L, count - 1);
        }
    }

    @Override
    public void removeCarFromParkingPoint(Long parkingPointId, Long carId) {
        try {
            if (EntityRepository.getEntityById(carId).getEntityType() != 2L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        String typeCar = EAVRepository.getEntityValueOfAttribute(carId, 1L).getStringValue();
        if (typeCar.equals("Passenger")) {
            Integer curCount = EAVRepository.getEntityValueOfAttribute(parkingPointId, 14L).getIntValue();
            Integer maxCount = EAVRepository.getEntityValueOfAttribute(parkingPointId, 8L).getIntValue();
            try {
                if (curCount.equals(maxCount))
                    throw new RuntimeException();
            } catch (Exception e) {
                System.out.println("In parking point nothing");
            };
            EAVRepository.updateEntityValueOfAttribute(parkingPointId, 14L, curCount + 1);
        } else if (typeCar.equals("Cargo")) {
            Integer curCount = EAVRepository.getEntityValueOfAttribute(parkingPointId, 13L).getIntValue();
            Integer maxCount = EAVRepository.getEntityValueOfAttribute(parkingPointId, 7L).getIntValue();
            try {
                if (curCount.equals(maxCount))
                    throw new RuntimeException();
            } catch (Exception e) {
                System.out.println("In parking point nothing");
            };
            EAVRepository.updateEntityValueOfAttribute(parkingPointId, 13L, curCount + 1);
        }
    }

    @Override
    public void deleteCarFromParkingPoint(Long parkingPointId, Long carId) {
        try {
            if (EntityRepository.getEntityById(carId).getEntityType() != 2L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        this.removeCarFromParkingPoint(parkingPointId, carId);
        EAVRepository.deleteChildEntity(parkingPointId, carId);
    }

    @Override
    public Integer giveNumberOfCargoSeats(Long parkingPointId) {
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return EAVRepository.getEntityValueOfAttribute(parkingPointId, 7L).getIntValue();
    }

    @Override
    public Integer giveNumberOfPassengerSeats(Long parkingPointId) {
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return EAVRepository.getEntityValueOfAttribute(parkingPointId, 8L).getIntValue();
    }

    @Override
    public Integer giveNumberOfFreeCargoSeats(Long parkingPointId) {
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return EAVRepository.getEntityValueOfAttribute(parkingPointId, 13L).getIntValue();
    }

    @Override
    public Integer giveNumberOfFreePassengerSeats(Long parkingPointId) {
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return EAVRepository.getEntityValueOfAttribute(parkingPointId, 14L).getIntValue();
    }

    @Override
    public Entity getParkingPointByName(String name) {
        try {
            Entity entity = EntityRepository.getEntitiesByName(name).get(0);
            if (EntityRepository.getEntityById(entity.getEntityId()).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
            return entity;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Entity getParkingPointByCarId(Long carId) {
        try {
            if (EntityRepository.getEntityById(carId).getEntityType() != 2L) {
                throw new IllegalArgumentException();
            };
            Entity entity = EntityRepository.getEntityById(EAVRepository.getEntityParentsId(carId).get(0));
            if (EntityRepository.getEntityById(entity.getEntityId()).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
            return entity;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public ParkingPointPrimitive giveParkingPointInfo(Long parkingPointId) {
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return ParkingPointPrimitive.builder()
                .parkingPoint(EntityRepository.getEntityById(parkingPointId))
                .numberOfCargoAll(this.giveNumberOfCargoSeats(parkingPointId))
                .numberOfCargoFree(this.giveNumberOfFreeCargoSeats(parkingPointId))
                .numberOfPassengerAll(this.giveNumberOfPassengerSeats(parkingPointId))
                .numberOfPassengerFree(this.giveNumberOfFreePassengerSeats(parkingPointId))
                .build();
    }

    @Override
    public List<Entity> giveAllParkingPoints() {
        return EntityRepository.getEntityByTypeId(3L); // 3 - Parking points
    }

    @Override
    public void updateCarParkingPoint(Long carId, Long parkingPointId) {
        try {
            EntityRepository.getEntityById(carId);
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        EAVRepository.updateEntityValueOfAttribute(parkingPointId, 6L, carId);
    }

    @Override
    public void updateParkingPointName(Long parkingPointId, String name) {
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
        EntityRepository.updateEntity(parkingPointId,
                Entity.builder()
                        .entityId(parkingPointId)
                        .name(name)
                        .entityType(3L)
                        .build()
        );
    }

    @Override
    public void updateParkingPointPassenger(Long parkingPointId, Integer numPassenger) {
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        if (numPassenger <= 0)
            throw new IllegalArgumentException();
        Integer curNumPassenger = EAVRepository.getEntityValueOfAttribute(parkingPointId, 8L).getIntValue();
        EAVRepository.updateEntityValueOfAttribute(parkingPointId, 8L, numPassenger);
        Integer curFreeNumPassenger = EAVRepository.getEntityValueOfAttribute(parkingPointId, 14L).getIntValue();
        EAVRepository.updateEntityValueOfAttribute(parkingPointId, 14L, curFreeNumPassenger + numPassenger - curNumPassenger);

    }

    @Override
    public void updateParkingPointCargo(Long parkingPointId, Integer numCargo) {
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        if (numCargo <= 0)
            throw new IllegalArgumentException();
        Integer curNumCargo = EAVRepository.getEntityValueOfAttribute(parkingPointId, 7L).getIntValue();
        EAVRepository.updateEntityValueOfAttribute(parkingPointId, 7L, numCargo);
        Integer curFreeNumCargo = EAVRepository.getEntityValueOfAttribute(parkingPointId, 13L).getIntValue();
        EAVRepository.updateEntityValueOfAttribute(parkingPointId, 13L, curFreeNumCargo + numCargo - curNumCargo);

    }

    @Override
    public void deleteParkingPoint(Long parkingPointId) {
        try {
            if (EntityRepository.getEntityById(parkingPointId).getEntityType() != 3L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        if (!giveNumberOfPassengerSeats(parkingPointId).equals(giveNumberOfFreePassengerSeats(parkingPointId)) ||
            !giveNumberOfCargoSeats(parkingPointId).equals(giveNumberOfFreeCargoSeats(parkingPointId))) {
            throw new IllegalCallerException();
        }
        EntityRepository.deleteEntityById(parkingPointId);
    }

    @Override
    public void createParkingPoint(ParkingPointPrimitive parkingPointPrimitive) {

        if (parkingPointPrimitive.getNumberOfPassengerAll() <= 0 || parkingPointPrimitive.getNumberOfCargoAll() <= 0)
            throw new IllegalArgumentException();
        Long parkId = EntityRepository.addEntity(3L, parkingPointPrimitive.getParkingPoint().getName());
        EAVRepository.setEntityValueOfAttribute(parkId, 7L, parkingPointPrimitive.getNumberOfCargoAll());
        EAVRepository.setEntityValueOfAttribute(parkId, 8L, parkingPointPrimitive.getNumberOfPassengerAll());
        EAVRepository.setEntityValueOfAttribute(parkId, 13L, parkingPointPrimitive.getNumberOfCargoAll());
        EAVRepository.setEntityValueOfAttribute(parkId, 14L, parkingPointPrimitive.getNumberOfPassengerAll());

    }
}
