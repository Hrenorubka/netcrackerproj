package com.example.projtry2.services.primitives;

import com.example.projtry2.entity.Entity;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class HistoryProfile {

    private Entity car;
    private String arrivalDate;
    private String departureDate;
    private String parkingPointName;
    private List<ServicePrimitive> servicePrimitive;
}
