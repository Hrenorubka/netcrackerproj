package com.example.projtry2.services.primitives;


import com.example.projtry2.entity.Entity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ParkingPointPrimitive {

    Entity parkingPoint;
    Integer numberOfCargoAll;
    Integer numberOfPassengerAll;
    Integer numberOfCargoFree;
    Integer numberOfPassengerFree;
}
