package com.example.projtry2.services.primitives;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdminUpdateCarInfo {
    private Long carId;
    private String parkingPoint;
    private String ownerPhone;
}
