package com.example.projtry2.services.primitives;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StringPrimitive {
    private String string;
}
