package com.example.projtry2.services.primitives;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserProfileInfo {
    private String username;
    private String phoneNumber;
    private String userLogin;
}
