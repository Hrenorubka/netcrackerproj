package com.example.projtry2.services.primitives;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EAVPrimitive {

    String entityName;
    String attrName;
    Integer intValue;
    Boolean boolValue;
    String stringValue;
    String refName;
}
