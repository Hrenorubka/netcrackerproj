package com.example.projtry2.services.primitives;

import com.example.projtry2.entity.Entity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CarGetPrimitive {

    List<ServicePrimitive> carService;
    Entity car;
    Entity parkingPoint;

}
