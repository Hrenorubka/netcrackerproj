package com.example.projtry2.services.primitives;

import com.example.projtry2.entity.Entity;
import com.example.projtry2.entity.User;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class AdminCarInfo {

    Entity car;
    Entity parkingPoint;
    User user;
    String depDate;
    String arrivalDate;
}
