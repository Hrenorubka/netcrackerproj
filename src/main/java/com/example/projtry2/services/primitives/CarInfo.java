package com.example.projtry2.services.primitives;


import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class CarInfo {
    private String carNumber;
    private Date dateEvent;
    private List<Long> listServicesId;
    private Boolean inPlace;
    private String carType;
    private String parkingPoint;
    private String phoneNumber;
}
