package com.example.projtry2.services.primitives;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class DepartureDatePrimitive {
    private Long carId;
    private Date departureDate;
}
