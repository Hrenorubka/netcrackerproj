package com.example.projtry2.services;

import com.example.projtry2.entity.Entity;
import com.example.projtry2.repository.*;
import com.example.projtry2.services.primitives.ServicePrimitive;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerCarService implements CarService {


    private EntityAttributeRepository EAVRepository;
    private AttributeRepository AttributeRepository;
    private EntityRepository EntityRepository;


    @Autowired
    public CustomerCarService(EntityAttributeRepository EAVRepository, AttributeRepository AttributeRepository,
                              EntityRepository EntityRepository) {
        this.EntityRepository = EntityRepository;
        this.AttributeRepository = AttributeRepository;
        this.EAVRepository = EAVRepository;
    }

    @Override
    public Integer getCostOfService(Long serviceId) {
        try {
            if (EntityRepository.getEntityById(serviceId).getEntityType() != 1L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        Long costId = AttributeRepository.getAttributeByName("Cost").getAttrId();
        return EAVRepository.getEntityValueOfAttribute(serviceId, costId).getIntValue();
    }

    @Override
    public Entity getServiceByName(String name) {
        Entity entity = EntityRepository.getEntitiesByName(name).get(0);
        if (EntityRepository.getEntityById(entity.getEntityId()).getEntityType() != 1L) {
            throw new IllegalArgumentException();
        };
        return entity;
    }

    @Override
    public List<ServicePrimitive> getAllCarService() {
        List<Entity> entityServices = EntityRepository.getEntityByTypeId(1L); // 1 -> EntityService
        List<ServicePrimitive> allCarServices = new ArrayList<>();
        for (Entity a : entityServices) {
            Long idService = a.getEntityId();
            String nameService = a.getName();
            Integer costService = getCostOfService(a.getEntityId());
            ServicePrimitive inp = ServicePrimitive.builder()
                    .id(idService)
                    .name(nameService)
                    .cost(costService)
                    .flag(false)
                    .build();
            allCarServices.add(inp);
        }
        return allCarServices;
    }

    @Override
    public void updateCostOfService(Long serviceId, Integer newCost) {
        try {
             if (EntityRepository.getEntityById(serviceId).getEntityType() != 1L) {
                 throw new IllegalArgumentException();
             };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        if (newCost <= 0)
            throw new IllegalArgumentException();
        EAVRepository.updateEntityValueOfAttribute(serviceId, 5L, newCost);
    }

    @Override
    public Entity createNewService(ServicePrimitive servicePrimitive) {
        if (servicePrimitive.getCost() <= 0)
            throw new IllegalArgumentException();
        Long out = EntityRepository.addEntity(1L, servicePrimitive.getName());
        EAVRepository.setEntityValueOfAttribute(out, 5L, servicePrimitive.getCost());
        return EntityRepository.getEntityById(out);
    }

    @Override
    public void deleteServiceById(Long serviceId)
    {
        try {
            if (EntityRepository.getEntityById(serviceId).getEntityType() != 1L) {
                throw new IllegalArgumentException();
            };
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        EntityRepository.deleteEntityById(serviceId);
    }
}
