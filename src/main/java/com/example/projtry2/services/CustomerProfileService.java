package com.example.projtry2.services;

import com.example.projtry2.entity.User;
import com.example.projtry2.repository.UserRepository;
import com.example.projtry2.services.primitives.HistoryProfile;
import com.example.projtry2.services.primitives.ServicePrimitive;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class CustomerProfileService implements ProfileService{

    private UserRepository userRepository;
    private Car car;

    @Autowired
    public CustomerProfileService(UserRepository userRepository, Car car) {
        this.car = car;
        this.userRepository = userRepository;
    }

    @Override
    public List<HistoryProfile> getHistoryProfile(String userLogin) {
        try {
            userRepository.findByLogin(userLogin);
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
        User user = userRepository.findByLogin(userLogin);
        List<Long> carsId = userRepository.getAllUserCar(user.getId());
        List<HistoryProfile> out = new ArrayList<>();
        for (Long id : carsId) {
            out.add(HistoryProfile.builder()
                    .car(car.getCarById(id))
                    .servicePrimitive(car.getServicePrimitiveOfCar(id))
                    .arrivalDate(car.getCarArrivalDate(id))
                    .departureDate(car.getCarDepartureDate(id))
                    .parkingPointName(car.getCarParkingPoint(id).getName())
                    .build());
        }
        return out;
    }
}
