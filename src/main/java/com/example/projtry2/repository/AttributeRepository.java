package com.example.projtry2.repository;

import com.example.projtry2.entity.Attribute;

public interface AttributeRepository {

    public Attribute getAttributeById(long attributeId);

    public String getAttributeType(Attribute attribute);

    public Attribute getAttributeByName(String name);

    public String addAttribute(Long attributeType, String name);

    public String updateAttribute(Attribute updatedAttribute);

    public String deleteAttribute(Long attributeId);

}
