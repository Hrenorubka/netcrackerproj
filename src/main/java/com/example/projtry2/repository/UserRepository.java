package com.example.projtry2.repository;

import com.example.projtry2.entity.User;

import java.util.List;

public interface UserRepository {

    User findByLogin(String login);

    User findByPhone(String phoneNumber);

    User save(User user);

    List<User> getAllUsers();

    List<Long> getAllUserCar(Long userId);

    User getUserById(Long userId);

    User getUserByCarId(Long carId);

    Long setGuestByPhoneNumber(String phoneNumber);

    String setCarToUser(Long idUser, Long idCar);

    String removeUserCar(Long idUser, Long idCar);

}
