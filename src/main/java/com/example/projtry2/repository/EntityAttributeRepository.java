package com.example.projtry2.repository;

import com.example.projtry2.entity.Value;

import java.util.List;

public interface EntityAttributeRepository {

    public List<Value> getEntityValuesOfAttributes(Long entityId);

    public List<Long> getEntityNestedEntitiesId(Long entityId);

    public List<Long> getEntityParentsId(Long refEntityId);

    public Value getEntityValueOfAttribute(Long entityId, Long attributeId);

    public <T> String setEntityValueOfAttribute(Long entityId, Long attributeId, T value);

    public <T> String updateEntityValueOfAttribute(Long entityId, Long attributeId, T value);

    public String deleteEntityValue(Long entityId, Long attributeId);

    public String deleteChildEntity(Long entityId, Long childId);
}
