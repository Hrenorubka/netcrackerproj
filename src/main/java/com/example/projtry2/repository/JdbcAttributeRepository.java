package com.example.projtry2.repository;

import com.example.projtry2.entity.Attribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class JdbcAttributeRepository implements AttributeRepository {

    private final JdbcTemplate jdbc;

    @Autowired
    public JdbcAttributeRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Attribute getAttributeById(long attributeId) {
        try {
            return jdbc.queryForObject("select attr_id, type_id, name from public.attribute " +
                    "where attr_id = ?", this::mapRowToAttribute, attributeId);
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String getAttributeType(Attribute attribute) {
        try {
            return jdbc.queryForObject("select name from public.attribute_type " +
                    "where attribute_type_id = ?", this::mapRowToString, attribute.getAttrType());
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Attribute getAttributeByName(String name) {
        try {
            return jdbc.queryForObject("select * from public.attribute " +
                    "where name = ?", this::mapRowToAttribute, name);
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String addAttribute(Long attributeType, String name) {
        try {
            jdbc.update("insert into public.attribute (name, type_id) values (?, ?)", name, attributeType);
            return "Ok";
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String updateAttribute(Attribute updatedAttribute) {
        try {
            jdbc.update("update public.attribute set type_id = ?, name = ? where attr_id = ?",
                    updatedAttribute.getAttrType(), updatedAttribute.getName(), updatedAttribute.getAttrId());
            return "Ok" ;
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String deleteAttribute(Long attributeId) {
        try {
            jdbc.update("delete from public.attribute where attr_id = ?", attributeId);
            return "Ok";
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }


    private Attribute mapRowToAttribute (ResultSet rs, int rowNum) throws SQLException {
        return Attribute.builder()
                .attrId(rs.getLong("attr_id"))
                .attrType(rs.getInt("type_id"))
                .name(rs.getString("name"))
                .build();
//        return new Attribute(
//                rs.getLong("attr_id"),
//                rs.getInt("type_id"),
//                rs.getString("name")
//        );
    }

    private String mapRowToString (ResultSet rs, int rowNum) throws SQLException {
        return rs.getString("name");
    }
}
