package com.example.projtry2.repository;

import com.example.projtry2.entity.RolesUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class JdbcRoleUserRepository implements RoleUserRepository{

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcRoleUserRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public RolesUser findByName(String name) {
        return jdbcTemplate.queryForObject("select * from public.roles where name = ?",
                this::mapRowToRolesUser, name);
    }

    @Override
    public RolesUser findById(Long id) {
        return jdbcTemplate.queryForObject("select * from public.roles where role_id = ?",
                this::mapRowToRolesUser, id);
    }

    private RolesUser mapRowToRolesUser(ResultSet rs, int rowNum) throws SQLException {
        return RolesUser.builder()
                .id(rs.getLong("role_id"))
                .name(rs.getString("name"))
                .build();
    }
}
