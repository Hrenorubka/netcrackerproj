package com.example.projtry2.repository;

import com.example.projtry2.entity.Attribute;
import com.example.projtry2.entity.Entity;
import com.example.projtry2.entity.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class JdbcEntityAttributeRepository implements EntityAttributeRepository{

    private final JdbcEntityRepository jdbcEntityRepository;
    private final JdbcAttributeRepository jdbcAttributeRepository;
    private final JdbcTemplate jdbcValue;

    @Autowired
    public JdbcEntityAttributeRepository(JdbcEntityRepository jdbcEntityRepository,
                                         JdbcAttributeRepository jdbcAttributeRepository,
                                         JdbcTemplate jdbcValue) {
        this.jdbcAttributeRepository = jdbcAttributeRepository;
        this.jdbcEntityRepository = jdbcEntityRepository;
        this.jdbcValue = jdbcValue;
    }

    @Override
    public List<Value> getEntityValuesOfAttributes(Long entityId) {
        try {
            Entity curEntity = jdbcEntityRepository.getEntityById(entityId);
            List<Long> attributesId = jdbcEntityRepository.getAllEntitiesAttributeId(entityId);
            List<Attribute> entitiesAttributes = new ArrayList<Attribute>();
            for (Long a : attributesId) {
                entitiesAttributes.add(jdbcAttributeRepository.getAttributeById(a));
            }
            List<Value> out = new ArrayList<Value>();
            for (Attribute curAttribute : entitiesAttributes) {
                Value next = jdbcValue.queryForObject(
                        "select * from public.value where entity_id = ? and attr_id = ?",
                        new RowMapper<Value>() {
                            @Override
                            public Value mapRow(ResultSet rs, int rowNum) throws SQLException {
                                Value outValue = Value.builder()
                                        .entityName(curEntity.getName())
                                        .attributeName(curAttribute.getName())
                                        .build();
                                switch (curAttribute.getAttrType()) {
                                    case 1:
                                        outValue.setIntValue(rs.getInt("int_value"));
                                        break;
                                    case 2:
                                        outValue.setDateValue(rs.getString("date_value")); // Dont work;
                                        break;
                                    case 3:
                                        outValue.setBoolValue(rs.getBoolean("bool_value"));
                                        break;
                                    case 4:
                                        outValue.setStringValue(rs.getString("str_value"));
                                        break;
                                    case 5:
                                        outValue.setRefValue(rs.getLong("ref_value"));
                                        break;
                                    default:
                                        System.out.println("Error");
                                        break;
                                }
                                return outValue;
                            }
                        },
                        curEntity.getEntityId(), curAttribute.getAttrId()
                );
                out.add(next);
            }
            return out;
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<Long> getEntityNestedEntitiesId(Long entityId) {
        try {
            List<Long> out = new ArrayList<Long>();
            out = jdbcValue.query(
                    "select ref_value from public.value where entity_id = ? and attr_id = 6",
                    new RowMapper<Long>() {
                        @Override
                        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                            return rs.getLong("ref_value");
                        }
                    },
                    entityId
            );
            return out;
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<Long> getEntityParentsId(Long refEntityId) {
        try {
            List<Long> out = new ArrayList<>();
            out = jdbcValue.query(
                    "select entity_id from public.value where ref_value = ?",
                    new RowMapper<Long>() {
                        @Override
                        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                            return rs.getLong("entity_id");
                        }
                    },
                    refEntityId
            );
            return out;
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Value getEntityValueOfAttribute(Long entityId, Long attributeId) {
        try {
            Entity curEntity = jdbcEntityRepository.getEntityById(entityId);
            Attribute curAttribute = jdbcAttributeRepository.getAttributeById(attributeId);
            return jdbcValue.queryForObject(
                    "select * from public.value where entity_id = ? and attr_id = ?",
                    new RowMapper<Value>() {
                        @Override
                        public Value mapRow(ResultSet rs, int rowNum) throws SQLException {
                            return Value.builder()
                                    .entityName(curEntity.getName())
                                    .attributeName(curAttribute.getName())
                                    .intValue(rs.getInt("int_value"))
                                    .dateValue(rs.getString("date_value"))
                                    .boolValue(rs.getBoolean("bool_value"))
                                    .stringValue(rs.getString("str_value"))
                                    .refValue(rs.getLong("ref_value"))
                                    .build();
                        }
                    }, entityId, attributeId
            );
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public <T> String setEntityValueOfAttribute(Long entityId, Long attributeId, T value) {
        Attribute curAttribute = jdbcAttributeRepository.getAttributeById(attributeId);
        if (curAttribute == null) {return null;}
        String SQL = "insert into public.value (entity_id, attr_id, ";
        SQL = getNameOfTypeAttribute(curAttribute, SQL);
        SQL += ") values (?, ?, ?)";
        try {
            jdbcValue.update(SQL, entityId, attributeId, value);
            return "Ok";
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public <T> String updateEntityValueOfAttribute(Long entityId, Long attributeId, T value) {
        Attribute curAttribute = jdbcAttributeRepository.getAttributeById(attributeId);
        if (curAttribute == null) {
            return "Incorrect attribute";
        }
        String SQL = "update public.value set ";
        SQL = getNameOfTypeAttribute(curAttribute, SQL);
        SQL += " = ? where entity_id = ? and attr_id = ?";
        try {
            jdbcValue.update(SQL, value, entityId, attributeId);
            return "Ok";
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    private String getNameOfTypeAttribute(Attribute curAttribute, String SQL) {
        switch (curAttribute.getAttrType()) {
            case 1: SQL += "int_value";
                break;
            case 2: SQL += "date_value";
                break;
            case 3: SQL += "bool_value";
                break;
            case 4: SQL += "str_value";
                break;
            case 5: SQL += "ref_value";
                break;
            default: System.out.println("Error");
                break;
        }
        return SQL;
    }

    @Override
    public String deleteEntityValue(Long entityId, Long attributeId) {
        try {
            jdbcValue.update("delete from public.value where entity_id = ? and attr_id = ?", entityId, attributeId);
            return "Ok";
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String deleteChildEntity(Long entityId, Long childId) {
        try {
            jdbcValue.update("delete from public.value where entity_id = ? and attr_id = 6 and ref_value = ?",
                    entityId, childId);
            return "Ok";
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }
}
