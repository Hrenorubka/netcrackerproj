package com.example.projtry2.repository;

import com.example.projtry2.entity.RolesUser;

public interface RoleUserRepository {

    public RolesUser findByName(String name);

    public RolesUser findById(Long id);

}
