package com.example.projtry2.repository;

import com.example.projtry2.entity.Entity;

import java.util.List;

public interface EntityRepository {

    public Entity getEntityById(long entityId);

    public List<Entity> getAllEntity();

    public String deleteEntityById(long entityId);

    public Entity addEntityWithId(Entity inpEntity);

    public Long addEntity(long inpType, String inpName);

    public String updateEntity(long entityId, Entity inpEntity);

    public List<Entity> getEntityByTypeId(long typeId);


    public List<Long> getAllEntitiesAttributeId(long entityId);

    public List<Entity> getEntitiesByName(String entityName);
}
