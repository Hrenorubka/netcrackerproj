package com.example.projtry2.repository;

import com.example.projtry2.entity.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

@Repository
public class JdbcEntityRepository implements EntityRepository {

    private final JdbcTemplate jdbc;


    @Autowired
    public JdbcEntityRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Entity getEntityById(long entityId) {

        return jdbc.queryForObject("select entity_id, type_id, name from public.entity " +
                    "where entity_id = ?", this::mapRowToEntity, entityId);
    }

    @Override
    public List<Entity> getAllEntity() {
        try {
            return jdbc.query("select entity_id, type_id, name from public.entity", this::mapRowToEntity);
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String deleteEntityById(long entityId) {
        try {
            jdbc.update("delete from public.entity where entity_id = ?", entityId);
            return "Ok";
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Entity addEntityWithId(Entity inpEntity) {
        try {
            jdbc.update("insert into public.entity (entity_id, type_id, name) " +
                            "values (?, ?, ?)",
                    inpEntity.getEntityId(), inpEntity.getEntityType(), inpEntity.getName());
            return inpEntity;
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Long addEntity(long inpType, String name) {
        String INSERT_MESS_SQL = "insert into public.entity (type_id, name) values (?, ?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbc.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                PreparedStatement ps = conn.prepareStatement(INSERT_MESS_SQL, new String[] {"entity_id"});
                ps.setLong(1, inpType);
                ps.setString(2, name);
                return ps;
            }
        }, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public String updateEntity(long entityId, Entity inpEntity) {
        try {
            jdbc.update("update public.entity set type_id = ?, name = ? where entity_id = ?",
                    inpEntity.getEntityType(), inpEntity.getName(), inpEntity.getEntityId());
            return "Ok";
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<Entity> getEntityByTypeId(long typeId) {
        try {
            return jdbc.query("select * from public.entity where type_id = ?",
                    this::mapRowToEntity, typeId);
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }


    @Override
    public List<Long> getAllEntitiesAttributeId(long entityId) {
        try {
            Entity curEntity = getEntityById(entityId);
            return jdbc.query("select attr_id from public.entity_type_attribute " +
                    "where entity_type_id = ?", this::mapRowToAttributeId, entityId);
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<Entity> getEntitiesByName(String entityName) {
        try {
            return jdbc.query("select * from public.entity where name = ?", this::mapRowToEntity, entityName);
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    private Long mapRowToAttributeId(ResultSet rs, int rowNum) throws SQLException {
        return rs.getLong("attr_id");
    }

    private Entity mapRowToEntity (ResultSet rs, int rowNum) throws SQLException {
        return Entity.builder()
                .entityId(rs.getLong("entity_id"))
                .entityType(rs.getLong("type_id"))
                .name(rs.getString("name"))
                .build();
    }
}

