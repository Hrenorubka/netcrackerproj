package com.example.projtry2.repository;

import com.example.projtry2.entity.RolesUser;
import com.example.projtry2.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class JdbcUserRepository implements UserRepository{

    private final JdbcTemplate jdbc;
    private final RoleUserRepository roleUserRepository;

    @Autowired
    public JdbcUserRepository(JdbcTemplate jdbc, RoleUserRepository roleUserRepository) {
        this.jdbc = jdbc;
        this.roleUserRepository = roleUserRepository;
    }

    @Override
    public User findByLogin(String login) {
        return jdbc.queryForObject("select * from public.user " +
                "where user_login = ?", this::mapToRowUser, login);
    }

    @Override
    public User findByPhone(String phoneNumber) {
        return jdbc.queryForObject("select * from public.user " +
                "where phone_number = ?", this::mapToRowUser, phoneNumber);
    }

    @Override
    public User save(User user) {
        try {
            if (user.getLogin() != null)
                this.findByLogin(user.getLogin());
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
        jdbc.update("insert into public.user " +
                        "(user_login, user_password, user_name, user_enabled, phone_number, role_id)" +
                        " values (?, ?, ?, ?, ?, ?)",
                user.getLogin(), user.getPassword(), user.getUsername(),
                user.getEnabled(), user.getPhoneNumber(), user.getRole().getId());
        return findByLogin(user.getLogin());
    }

    @Override
    public List<User> getAllUsers() {
        return jdbc.query("select * from public.user " +
                "where role_id != 1 and user_enabled = true",
                new RowMapper<User>() {
            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {
                return User.builder()
                        .id(resultSet.getLong("user_id"))
                        .enabled(true)
                        .login(resultSet.getString("user_login"))
                        .password(resultSet.getString("user_password"))
                        .username(resultSet.getString("user_name"))
                        .phoneNumber(resultSet.getString("phone_number"))
                        .role(roleUserRepository.findById(resultSet.getLong("role_id")))
                        .build();
            }
        });
    }

    @Override
    public List<Long> getAllUserCar(Long userId) {
        return jdbc.query("select entity_id from public.user_entity where user_id = ?", new RowMapper<Long>() {
            @Override
            public Long mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getLong("entity_id");
            }
        }, userId);
    }

    @Override
    public User getUserById(Long userId) {
        return jdbc.queryForObject("select * from public.user where user_id = ?",
                this::mapToRowUser, userId);
    }

    @Override
    public User getUserByCarId(Long carId) {
        Long userId = jdbc.queryForObject("select * from public.user_entity where entity_id = ?", new RowMapper<Long>() {
            @Override
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getLong("user_id");
            }
        }, carId);
        return getUserById(userId);
    }

    @Override
    public Long setGuestByPhoneNumber(String phoneNumber) {
        jdbc.update("insert into public.user (user_enabled, phone_number, role_id) values (true, ?, 3)", phoneNumber);
        return this.findByPhone(phoneNumber).getId();
    }

    @Override
    public String setCarToUser(Long idUser, Long idCar) {
        try {
            jdbc.update("insert into public.user_entity (entity_id, user_id)" +
                    "values (?, ?)", idCar, idUser);
            return "Ok";
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String removeUserCar(Long idUser, Long idCar) {
        try {
            jdbc.update("delete from public.user_entity where entity_id = ? AND user_id = ?", idCar, idUser);
            return "Ok";
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }


    private User mapToRowUser(ResultSet rs, int rowNum) throws SQLException {
        RolesUser roleUser = roleUserRepository.findById(rs.getLong("role_id"));
        return User.builder()
                .id(rs.getLong("user_id"))
                .login(rs.getString("user_login"))
                .password(rs.getString("user_password"))
                .username(rs.getString("user_name"))
                .enabled(rs.getBoolean("user_enabled"))
                .phoneNumber(rs.getString("phone_number"))
                .role(roleUser)
                .build();
    }
}
