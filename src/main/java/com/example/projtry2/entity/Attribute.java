package com.example.projtry2.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Attribute {


    private Long attrId;
    private Integer attrType;
    private String name;

}
