package com.example.projtry2.entity;


import lombok.Data;

@Data
public class EntityType {

    public Long id;
    public String name;
}
