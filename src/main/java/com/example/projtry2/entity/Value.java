package com.example.projtry2.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Value{

    private String entityName;
    private String attributeName;
    private Integer intValue;
    private String dateValue;
    private Boolean boolValue;
    private String stringValue;
    private Long refValue;

//    public Value(String entityName, String attributeName) {
//        this.entityName = entityName;
//        this.attributeName = attributeName;
//        this.intValue = null;
//        this.dateValue = null;
//        this.boolValue = null;
//        this.stringValue = null;
//        this.refValue = null;
//    }
}
