package com.example.projtry2.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User{

    private Long id;
    private String password;
    private String login;
    private String username;
    private String phoneNumber;
    private Boolean enabled;
    private RolesUser role;



}
