package com.example.projtry2.entity;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Entity {

    private Long entityId;
    //private EntityType entityType;
    private Long entityType;
    private String name;


}
