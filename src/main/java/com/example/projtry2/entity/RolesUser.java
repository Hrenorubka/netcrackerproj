package com.example.projtry2.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RolesUser {

    private Long id;
    private String name;

}
